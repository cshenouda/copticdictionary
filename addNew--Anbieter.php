  <?php
    session_start();
    if (isset($_SESSION['parseData']['user']) && $_SESSION['level'] == "admin") 
    {
  ?>
  <script type="text/javascript" src="polymer/bower_components/webcomponentsjs/webcomponents-lite.js"></script>
  <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
  
  <link rel="import" href="polymer/bower_components/polymer/polymer.html">
  <link rel="import" href="polymer/bower_components/paper-button/paper-button.html">
  <link href="polymer/bower_components/paper-drawer-panel/paper-drawer-panel.html" rel="import">
  <link href="polymer/bower_components/paper-header-panel/paper-header-panel.html" rel="import">
  <link href="polymer/bower_components/paper-toolbar/paper-toolbar.html" rel="import">
  <link href="polymer/bower_components/paper-icon-button/paper-icon-button.html" rel="import">
  <link href="polymer/bower_components/paper-material/paper-material.html" rel="import">
  <link href="polymer/bower_components/paper-menu/paper-menu.html" rel="import"> 
  <link href="polymer/bower_components/paper-item/paper-item.html" rel="import">
  <link href="polymer/bower_components/paper-input/paper-input.html" rel="import">
  <link href="polymer/bower_components/paper-input/paper-textarea.html" rel="import">
  <link href="polymer/bower_components/iron-icons/iron-icons.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-card.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-column.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/datatable-icons.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-edit-dialog.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-styles.html" rel="import">
  <link rel="import" href="polymer/bower_components/paper-dialog/paper-dialog.html">
  <link rel="import" href="polymer/bower_components/paper-dialog-scrollable/paper-dialog-scrollable.html">
  <link rel="import" href="polymer/bower_components/neon-animation/neon-animations.html">
  
 
<style>
/*body{
  overflow:hidden;
}
paper-card{
  margin-bottom:20px;
  display:block;
}
paper-card {
  --paper-card-header-text: {
    font-family: 'Roboto', 'Noto', sans-serif;
    font-weight: normal;
    font-size: 20px;
  }
}*/


</style>
<body>
  
  <div id="main">
    <template is="dom-bind" id="app">
    <div class="result"></div>
    <h2>Anbieter hinzufügen</span></h2>
      <paper-input required value="{{name}}" label="Name"></paper-input>
      <paper-input required value="{{userName}}" label="Benutzername"></paper-input>
      <paper-textarea required value="{{emailAdresse}}" label="Emailadresse"></paper-textarea>
      <paper-input value="{{bild}}" label="Dateinamen (z.B. 40tage.jpg oder dummy.jpg) eingeben"></paper-input>
    <div class="buttons">
      <paper-button dialog-confirm on-tap="confirmSave">Speichern</paper-button>
    </div>
  

<paper-dialog id="confirmationDialog" with-backdrop style="max-width:500px;min-height:300px;">
  <!-- <template is="dom-repeat" items="{{selectedItems}}" filter="{{onlyFirst}}"> -->
    <h2>Speichern</h2>
      <paper-dialog-scrollable>
        Sind Sie sicher, dass Sie diese Änderungen speichern wollen?<br>Änderungen sind nicht wiederherstellbar.
      </paper-dialog-scrollable>
    <div class="buttons">
      <paper-button dialog-confirm on-tap="saveProfile">Ja</paper-button>
      <paper-button dialog-dismiss>Nein</paper-button>
    </div>
  <!-- </template> -->
</paper-dialog>
    </template>
  </div>
  
  <script>

  var app = document.querySelector('#app');
  // app.anbieter = $.post( "getdata.php", {'m': 'anbieter'}, function( result) {
  //   app.anbieter = jQuery.makeArray( result );
  // }, "json");  

      
  app.confirmSave = function (e) {
    this.$.confirmationDialog.open();
  }

  app.saveProfile = function (e) {
    console.log("save " +app.name);
    $.post( "adddata.php", {'m': 'addProfile', 'userName': app.userName, 'name': app.name, 'emailAdresse': app.emailAdresse, 'bild': app.bild}, function( result) {
      if (result.error != null){
        $('.result').html('Es ist ein Fehler aufgetreten: ' +result.error);
      }else {
        $('.result').html(result.welcomeText);  
      }
     
//       $('.main').html('Sehr geehrtes Team ' +result.name +', 
// <br>
// Wir freuen uns sehr, dass Sie deutschlandbetet.de als Plattform nutzen möchten.
// Die Admin-Oberfläche zur Erstellung Ihrer Anliegen finden Sie hier:
// www.deutschlandbetet.de/admin

// Ihre Zugangsdaten:
// Benutzername: '+result.username
// +'Passwort: ' +result.passwort

// +'Anbei finden Sie eine Anleitung, wie Anliegen erstellt, editiert, usw. werden. Die gleiche Anleitung finden Sie auch im Adminbereich unter "Hilfe".
// Wir haben ein Profil und eine Aktion für Sie angelegt. Die weiteren Details dazu (Adresse, Beschreibungen, etc.) können Sie leicht selbst vervollständigen, siehe Anleitung.
// Ihre Aktion ist momentan nicht öffentlich sichtbar. Sobald Sie live gehen möchten, schreiben Sie uns eine kurze Mail und ich schalte Sie frei.

// Ich bräuchte von Ihnen noch ein Logo für Sie als Anbieter und eins für die aktuell geplante Aktion. Diese können auch identisch sein. 
// Am besten ist es, wenn das Logo einen weißen Hintergrund hat. Es muss außerdem quadratisch sein und mindestens die Größe 128x128 Pixel haben.

// Falls Sie Fragen haben, stehen wir gerne zur Verfügung. 

// Freundliche Grüße,
//  - Deutschland betet Team ');
    }, "json");  
  }

  app.addAktion = function(e) {
    app.selectedItems = null;
    app.$.addDialogAktion.open();
  }
</script>

<?php
  }else if ($_SESSION['level'] != "admin"){
    echo 'Forbidden! Contact Admin.';
  }else{
    header('Location: login.php');
  }
?>
</body>