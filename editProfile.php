  <?php
    session_start();
    if (isset($_SESSION['parseData']['user'])) 
    {
  ?>
  <script type="text/javascript" src="polymer/bower_components/webcomponentsjs/webcomponents-lite.js"></script>
  
  <link rel="import" href="polymer/bower_components/polymer/polymer.html">
  <link rel="import" href="polymer/bower_components/paper-button/paper-button.html">
  <link href="polymer/bower_components/paper-drawer-panel/paper-drawer-panel.html" rel="import">
  <link href="polymer/bower_components/paper-header-panel/paper-header-panel.html" rel="import">
  <link href="polymer/bower_components/paper-toolbar/paper-toolbar.html" rel="import">
  <link href="polymer/bower_components/paper-icon-button/paper-icon-button.html" rel="import">
  <link href="polymer/bower_components/paper-material/paper-material.html" rel="import">
  <link href="polymer/bower_components/paper-menu/paper-menu.html" rel="import"> 
  <link href="polymer/bower_components/paper-item/paper-item.html" rel="import">
  <link href="polymer/bower_components/paper-input/paper-input.html" rel="import">
  <link href="polymer/bower_components/paper-input/paper-textarea.html" rel="import">
  <link href="polymer/bower_components/iron-icons/iron-icons.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-card.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-column.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/datatable-icons.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-edit-dialog.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-styles.html" rel="import">
  <link rel="import" href="polymer/bower_components/paper-dialog/paper-dialog.html">
  <link rel="import" href="polymer/bower_components/paper-dialog-scrollable/paper-dialog-scrollable.html">
  <link rel="import" href="polymer/bower_components/neon-animation/neon-animations.html">
  <link rel="import" href="polymer/bower_components/file-upload/file-upload.html">
 
<style>
/*body{
  overflow:hidden;
}
paper-card{
  margin-bottom:20px;
  display:block;
}
paper-card {
  --paper-card-header-text: {
    font-family: 'Roboto', 'Noto', sans-serif;
    font-weight: normal;
    font-size: 20px;
  }
}*/


</style>
<body>
  
  <div id="main">
    <template is="dom-bind" id="app">
  <!-- <template is="dom-repeat" items="{{anbieter}}"> -->
    <h2>Profil editieren</span></h2>
      <paper-input value="{{editProfile.name}}" label="Name"></paper-input>
      <paper-textarea value="{{editProfile.beschreibung}}" label="Beschreibung"></paper-textarea>
      <paper-textarea value="{{editProfile.spende_text}}" label="Spendentext"></paper-textarea>
      <paper-textarea value="{{editProfile.adresse}}" label="Adresse"></paper-textarea>
      <paper-textarea value="{{editProfile.emailAdresse}}" label="Emailadresse"></paper-textarea>
      <paper-textarea value="{{editProfile.www}}" label="Webseite"></paper-textarea>
      <paper-textarea value="{{editProfile.events}}" label="Events"></paper-textarea>
      <paper-textarea value="{{editProfile.tel}}" label="Telefon"></paper-textarea>
      <paper-textarea value="{{editProfile.spende_www}}" label="Spenden-Webseite"></paper-textarea>
      <?php
        if ($_SESSION['level'] == "admin") {
      ?>
        <paper-input value="{{bild}}" label="Bild"></paper-input>
      <?php
        }
      ?>
      <!-- <paper-input value="{{bild}}" label="Bild"></paper-input> -->
    <div class="buttons">
      <paper-button dialog-confirm on-tap="confirmSave">Speichern</paper-button>
    </div>
  <!-- </template> -->

<paper-dialog id="confirmationDialog" with-backdrop style="max-width:500px;min-height:300px;">
  <!-- <template is="dom-repeat" items="{{selectedItems}}" filter="{{onlyFirst}}"> -->
    <h2>Speichern</h2>
      <paper-dialog-scrollable>
        Sind Sie sicher, dass Sie diese Änderungen speichern wollen?<br>Änderungen sind nicht wiederherstellbar.
      </paper-dialog-scrollable>
    <div class="buttons">
      <paper-button dialog-confirm on-tap="saveProfile">Ja</paper-button>
      <paper-button dialog-dismiss>Nein</paper-button>
    </div>
  <!-- </template> -->
</paper-dialog>


 
        

    <div class="result">
      
    </div>
    </template>
  </div>
  
  <script>

  var app = document.querySelector('#app');
  var admin_profileId = "<?php echo $_GET['id']; ?>";
  if (admin_profileId == ""){
    // console.log('no admin');
    admin_profileId = null;
    app.anbieter = $.post( "getdata.php", {'m': 'anbieter'}, function( result) {
      app.anbieter = jQuery.makeArray( result );
      app.editProfile = app.anbieter[0];
    }, "json");  

  }else{
    console.log(admin_profileId);
    app.anbieter = $.post( "getdata.php", {'m': 'anbieter', 'anbieterId': admin_profileId}, function( result) {
      app.anbieter = jQuery.makeArray( result );
      app.editProfile = app.anbieter[0];
    }, "json");  

  }
  
      
  app.confirmSave = function (e) {
    this.$.confirmationDialog.open();
  }

  app.saveProfile = function (e) {
    console.log("save " +app.anbieter[0]);
      console.log(app.bild);
    $.post( "adddata.php", {'m': 'addProfile', 'id': app.editProfile.id, 'name': app.editProfile.name, 'beschreibung': app.editProfile.beschreibung, 'spende_text': app.editProfile.spende_text, 'adresse': app.editProfile.adresse, 'emailAdresse': app.editProfile.emailAdresse, 'www': app.editProfile.www, 'events': app.editProfile.events, 'tel': app.editProfile.tel, 'spende_www': app.editProfile.spende_www, 'bild': app.bild}, function( result) {
      console.log(result);
    }, "json");  
    // var editProfile = JSON.stringify(app.anbieter[0]);
    // $.post( "adddata.php", {'m': 'addProfile', 'anbieter': editProfile}, function( result) {
    //   console.log(result);
    // }, "json");  
  }

  app.addAktion = function(e) {
    app.selectedItems = null;
    app.$.addDialogAktion.open();
  }
</script>

<?php
  }else{
    header('Location: login.php');
  }
?>
</body>