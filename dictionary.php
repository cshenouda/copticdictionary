  <?php
    session_start();
    if (isset($_SESSION['parseData']['user'])) 
    {
  ?>
  <script type="text/javascript" src="polymer/bower_components/webcomponentsjs/webcomponents.js"></script>
  
  <link rel="import" href="polymer/bower_components/polymer/polymer.html">
  <link rel="import" href="polymer/bower_components/paper-button/paper-button.html">
  <link href="polymer/bower_components/paper-drawer-panel/paper-drawer-panel.html" rel="import">
  <link href="polymer/bower_components/paper-header-panel/paper-header-panel.html" rel="import">
  <link href="polymer/bower_components/paper-toolbar/paper-toolbar.html" rel="import">
  <link href="polymer/bower_components/paper-icon-button/paper-icon-button.html" rel="import">
  <link href="polymer/bower_components/paper-material/paper-material.html" rel="import">
  <link href="polymer/bower_components/paper-menu/paper-menu.html" rel="import"> 
  <link href="polymer/bower_components/paper-item/paper-item.html" rel="import">
  <link href="polymer/bower_components/paper-input/paper-input.html" rel="import">
  <link href="polymer/bower_components/paper-input/paper-textarea.html" rel="import">
  <link href="polymer/bower_components/iron-icons/iron-icons.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-card.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-column.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/datatable-icons.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-edit-dialog.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-styles.html" rel="import">
  <link href="polymer/bower_components/paper-dropdown-menu/paper-dropdown-menu.html" rel="import">
  <link rel="import" href="polymer/bower_components/paper-dialog/paper-dialog.html">
  <link rel="import" href="polymer/bower_components/paper-dialog-scrollable/paper-dialog-scrollable.html">
  <link rel="import" href="polymer/bower_components/neon-animation/neon-animations.html">
  <link rel="import" href="polymer/bower_components/paper-listbox/paper-listbox.html">
  <link rel="import" href="polymer/bower_components/paper-date-picker/paper-date-picker.html">
  
 

<style>
/*body{
  overflow:hidden;
}
paper-card{
  margin-bottom:20px;
  display:block;
}
paper-card {
  --paper-card-header-text: {
    font-family: 'Roboto', 'Noto', sans-serif;
    font-weight: normal;
    font-size: 20px;
  }
}*/

</style>
<style is="custom-style" include="paper-date-picker-dialog-style">
    section {
      margin: 24px;
    }
    
    @font-face { 
      font-family: Coptic; 
      src: url('newath.ttf'); 
    }

    #editCoptic, #addCoptic, #editExample, #addExample {
        --paper-input-container-input: {
          font-family: Coptic;
          font-size:20px;
        };
    }

    #tableCoptic {
          font-family: Coptic;
          font-size:20px;
    }
  </style>
<body>
  
  <div id="main">
    
    <template is="dom-bind" id="anliegenApp">




<paper-dialog id="datumDialog" class="paper-date-picker-dialog" modal
  on-iron-overlay-closed="dismissDialog">
  <paper-date-picker id="picker" date="{{selectedDate}}"></paper-date-picker>
  <div class="buttons">
    <paper-button dialog-dismiss>Cancel</paper-button>
    <paper-button dialog-confirm on-tap="datumSelected">OK</paper-button>
  </div>
</paper-dialog>

<paper-dialog id="editDialogAnliegen" with-backdrop style="width:80%;min-height:300px;">
  <paper-dialog-scrollable>
  <!-- <template id="editDialogAnliegenFields" is="dom-repeat" items="{{selectedItems}}" filter="{{onlyFirst}}"> -->
    <h2>Dictionary Entry #<span>{{editAnliegen.id}}</span></h2>
        <paper-input id="editCoptic" required auto-validate error-message="Coptic" value="{{editAnliegen.coptic}}" label="Coptic" ></paper-input>
        <paper-input id="editEnglish" required auto-validate error-message="English" value="{{editAnliegen.english}}" label="English"></paper-input>
        <paper-input id="editGerman" required auto-validate error-message="German" value="{{editAnliegen.german}}" label="German"></paper-input>
        <paper-input id="editFrench" required auto-validate error-message="French" value="{{editAnliegen.french}}" label="French"></paper-input>
        
        <paper-input id="editGrammar" required auto-validate error-message="Grammar" value="{{editAnliegen.grammar}}" label="Grammar"></paper-input>
        <paper-textarea id="editDescription" char-counter maxlength="2000" value="{{editAnliegen.description}}" label="Description"></paper-textarea>
        <paper-textarea id="editExample" required auto-validate error-message="Example" char-counter maxlength="300" value="{{editAnliegen.example}}" label="Example"></paper-textarea>
      
    <div class="buttons">
      <paper-button on-tap="confirmSave">Save</paper-button>
    </div>
  <!-- </template> -->
  </paper-dialog-scrollable>
</paper-dialog>

<paper-dialog id="addDialogAnliegen" with-backdrop style="width:80%;min-height:300px;">
  <!-- <template is="dom-repeat"> -->
    <paper-dialog-scrollable>
    <h2>New dictionary entry</span></h2>
        <paper-input id="addCoptic" required auto-validate error-message="Coptic" value="{{addAnliegenCoptic}}" label="Coptic"></paper-input>
        <paper-input id="addGrammar" required auto-validate error-message="Grammar" value="{{addAnliegenGrammar}}" label="Grammar"></paper-input>
        <paper-input id="addEnglish" required auto-validate error-message="English" value="{{addAnliegenEnglish}}" label="English"></paper-input>
        <paper-input id="addGerman" required auto-validate error-message="German" value="{{addAnliegenGerman}}" label="German"></paper-input>
        <paper-input id="addFrench" required auto-validate error-message="French" value="{{addAnliegenFrench}}" label="French"></paper-input>
        
        <paper-textarea id="addDescription" char-counter maxlength="2000" value="{{addAnliegenDescription}}" label="Description"></paper-textarea>
        <paper-textarea id="addExample" required auto-validate error-message="Example" char-counter maxlength="300" value="{{addAnliegenExample}}" label="Example"></paper-textarea>
      
    <div class="buttons">
      <paper-button on-tap="confirmSave">Save</paper-button>
    </div>
    </paper-dialog-scrollable>
  <!-- </template> -->
</paper-dialog>

<paper-dialog id="confirmationDialog" with-backdrop style="max-width:500px;min-height:300px;">
  <!-- <template is="dom-repeat"> -->
    <h2>Save</h2>
      <paper-dialog-scrollable>
        Are you sure you want to save these changes?<br>This cannot be undone.
     
    <div class="buttons">
      <paper-button dialog-confirm on-tap="saveAnliegen">Yes</paper-button>
      <paper-button dialog-dismiss>No</paper-button>
    </div>
     </paper-dialog-scrollable>
  <!-- </template> -->
</paper-dialog>

<paper-dialog id="confirmationDeleteDialog" with-backdrop style="max-width:500px;min-height:300px;">
  <!-- <template is="dom-repeat"> -->
    <h2>Delete</h2>
      <paper-dialog-scrollable>
        Are you sure you want to <span style="color:red">delete</span> this object?<br>This cannot be undone.
      
    <div class="buttons">
      <paper-button dialog-confirm on-tap="deleteAnliegen">Yes</paper-button>
      <paper-button dialog-dismiss>No</paper-button>
    </div>
     </paper-dialog-scrollable>
  <!-- </template> -->
</paper-dialog>
<paper-datatable-card id="datatableCardAnliegen" header="Dictionary" page-size="10" data-source="{{dataSource}}" id-property="id" selected-ids="{{selectedIds}}" style="width:99%">
          <div toolbar-main>
            <!-- <paper-input value="{{searchTerm}}" on-input="retrieveResults" label="Search..." style="display:inline-block" no-label-float>
              <div prefix>
                <iron-icon icon="search"></iron-icon>
              </div>
            </paper-input> -->
            <paper-icon-button icon="cached" on-tap="retrieveResults"></paper-icon-button>
            <paper-icon-button icon="add" on-tap="addAnliegen"></paper-icon-button>
          </div>
          <div toolbar-select>
            <paper-icon-button icon="delete" on-tap="deleteSelected"></paper-icon-button>
          </div>
          <div toolbar-select-single>
            <paper-icon-button icon="datatable:editable" on-tap="editDialogForSelected"></paper-icon-button>
          </div>
          <paper-datatable id="datatableAnliegen" style="width:99%" selectable multi-selection selected-items="{{selectedItems}}" on-row-tap="rowTapped">
            <div no-results>
              Loading or no more entries...
            </div>
            <paper-datatable-column header="ID" property="id" type="String" sortable>
            </paper-datatable-column>
           <!--  <template>
              <div>[[formatDate(item.datum.date)]]</div>
            </template> -->
            </paper-datatable-column>
            <paper-datatable-column header="Coptic" property="coptic" type="String" style="min-width: 120px">
              <template>
                <div id="tableCoptic">{{value}}</div>
              </template>
            </paper-datatable-column>
            <paper-datatable-column header="Grammar" property="grammar" type="String" style="min-width: 120px">
            </paper-datatable-column>
            <paper-datatable-column header="English" property="english" type="String" sortable style="min-width: 120px">
            </paper-datatable-column>
            <paper-datatable-column header="German" property="german" type="String" style="min-width: 120px">
            </paper-datatable-column>
            <paper-datatable-column header="French" property="french" type="String" style="min-width: 120px">
            </paper-datatable-column>
            <paper-datatable-column header="Description" property="description" type="String" style="min-width: 220px">
            </paper-datatable-column>
            <paper-datatable-column header="Example" property="example" type="String" style="min-width: 120px">
              <template>
                <div id="tableCoptic">{{value}}</div>
              </template>
            </paper-datatable-column>
            <paper-datatable-column header="Last updated" property="updatedAt" format-value="{{formatdate2}}" style="min-width: 120px" sortable>
            <!-- <template>
              <div>[[formatDate(item.updatedAt.date)]]</div>
            </template> -->
            </paper-datatable-column>
            <!-- <paper-datatable-column header="Bild" property="bild" sortable>
          <template>
                <div>
                  <img src="[[item.bild.file]]" style="width:32px;height:32px;border-radius:16px;vertical-align: middle;margin-right:4px;">
                </div>
              </template>
            </paper-datatable-column> -->
          </paper-datatable>
        </paper-datatable-card>



       
        

    <div class="result">
      
    </div>
    </template>
    
  </div>
  
  <script>

    document.addEventListener('WebComponentsReady', function() {
      var anliegenApp = document.querySelector('#anliegenApp');
      var firstStart = true;

      var evt = document.createEvent("Event");
      evt.initEvent("myEvent",true,true); 

      // anliegenApp.$.listboxAktionen.addEventListener('attached', function(){
      //   console.log('listbox attached');
      // });

      console.log('WebComponentsReady');
      anliegenApp.formatdate2 = function(str) {
        return jQuery.format.date(str, "dd.MM.yyyy");
      };

      document.dispatchEvent(evt);

      anliegenApp.capitalize = function(str){
      return str.toString().split(' ').map(function(str){
        return str.substr(0,1).toUpperCase() + str.substr(1, str.length - 1);
      }).join(' ');
    };
    
    anliegenApp.deleteSelected = function(){
      this.$.confirmationDeleteDialog.open();
    };

    anliegenApp.deleteAnliegen = function(e){
      var deleteAnliegenId = anliegenApp.selectedIds;
      console.log('delete '+deleteAnliegenId);
      $.post( "deletedata.php", {'m': 'deleteDictEntry', 'id': deleteAnliegenId}, function( result) {
          console.log(result);
          anliegenApp.set(anliegenApp.selectedItems, null);
          anliegenApp.retrieveResults();
        }, "json");  
    };

    anliegenApp.editDialogForSelected = function(){
      // this.$.editDialogAnliegen.opened = true;
      // anliegenApp.selectedItems = anliegenApp.selectedItems[0];
      anliegenApp.editAnliegen = anliegenApp.selectedItems[0];
      this.$.editDialogAnliegen.open();
    };

    anliegenApp.onlyFirst = function(item, index){
      // if(typeof index === 'undefined'){
      //   alert('Please see issue #34');
      // }
      // if(index == 0){
        return true;
      // }
    };

    anliegenApp.rowTapped = function(ev){
      if(anliegenApp.isDebouncerActive('dblclick'+ev.detail.item.id)){
        //double click
        anliegenApp.$.datatableCardAnliegen.deselectAll();
        anliegenApp.$.datatableCardAnliegen.select(ev.detail.item.id);
        anliegenApp.editDialogForSelected();
        ev.preventDefault();
        anliegenApp.cancelDebouncer('dblclick');
      }else{
        anliegenApp.debounce('dblclick'+ev.detail.item.id, function(){}, 300);
      }
    };

    anliegenApp.retrieveResults = function(ev){
      anliegenApp.$.datatableCardAnliegen.retrieveVisibleData();
    };

    anliegenApp.confirmSave = function (e) {
      var isValid = true;
      //edit
      if(anliegenApp.selectedItems != null){
        if (this.$.editCoptic.validate() == false) {
          isValid = false;
        }
        if (this.$.editGerman.validate() == false) {
          isValid = false;
        }
        if (this.$.editFrench.validate() == false) {
          isValid = false;
        }
        if (this.$.editEnglish.validate() == false) {
          isValid = false;
        } 
        if (this.$.editGrammar.validate() == false) {
          isValid = false;
        }
        if (this.$.editExample.validate() == false) {
          isValid = false;
        }
        if (this.$.editDescription.validate() == false) {
          isValid = false;
        }

        if (isValid == true){
          this.$.editDialogAnliegen.close();
        } 
      }
      //add
      else{
         if (this.$.addCoptic.validate() == false) {
          isValid = false;
        }
        if (this.$.addGerman.validate() == false) {
          isValid = false;
        }
        if (this.$.addFrench.validate() == false) {
          isValid = false;
        }
        if (this.$.addEnglish.validate() == false) {
          isValid = false;
        } 
        if (this.$.addGrammar.validate() == false) {
          isValid = false;
        }
        if (this.$.addExample.validate() == false) {
          isValid = false;
        }
        if (this.$.addDescription.validate() == false) {
          isValid = false;
        }
        
        if (isValid == true){
          this.$.addDialogAnliegen.close();
        } 
      }
      
      if (isValid == true){
        this.$.confirmationDialog.open();
      }
    };

    anliegenApp.saveAnliegen = function (e) {
      console.log(anliegenApp.selectedItems);
      if(anliegenApp.selectedItems != null){
        console.log("save " +anliegenApp.selectedIds[0]);
        var editAnliegen = anliegenApp.editAnliegen;
        $.post( "adddata.php", {'m': 'addDictEntry', 'id': anliegenApp.selectedIds[0], 'english': editAnliegen.english, 'coptic': editAnliegen.coptic, 'german': editAnliegen.german, 'french': editAnliegen.french, 'description': editAnliegen.description, 'example': editAnliegen.example, 'grammar': editAnliegen.grammar}, function( result) {
          console.log(result);
          anliegenApp.retrieveResults();
        }, "json");  
      }else {
        console.log("saving new Anliegen " +anliegenApp.addAnliegenTitel);
        console.log(anliegenApp.addAnliegenText);
        // if (anliegenApp.addAnliegenText == null){
        //   anliegenApp.addAnliegenText = "";
        // }
        $.post( "adddata.php", {'m': 'addDictEntry', 'english': anliegenApp.addAnliegenEnglish, 'coptic': anliegenApp.addAnliegenCoptic, 'german': anliegenApp.addAnliegenGerman, 'french': anliegenApp.addAnliegenFrench, 'description': anliegenApp.addAnliegenDescription, 'example': anliegenApp.addAnliegenExample, 'grammar': anliegenApp.addAnliegenGrammar}, function( result) {
          console.log(result);
          anliegenApp.retrieveResults();
        }, "json");
      }
    };

    anliegenApp.aktionSelected = function (e, item) {
      console.log('item' +item +'selected');
      if (anliegenApp.firstStart){
        firstStart = false;
      }else {
        anliegenApp.$.datatableCardAnliegen.retrieveVisibleData();  
      }
    };

    anliegenApp.addAnliegen = function(e) {
      anliegenApp.selectedItems = null;
      anliegenApp.$.addDialogAnliegen.open();
    };

    anliegenApp.selectDate = function(e) {
      var date;
      if(anliegenApp.selectedItems != null){
        if (anliegenApp.selectedItems.constructor === Array){
          date = anliegenApp.selectedItems[0].datum;
        }else {
          date = anliegenApp.selectedItems.datum;
        }
      }else if (anliegenApp.addAnliegenDatum.length < 2){
        date = new Date();
      }else {
        date = anliegenApp.addAnliegenDatum
      }
      anliegenApp.selectedDate = new Date(date);
      this.$.datumDialog.open();
    };

    anliegenApp.datumSelected = function(e) {
      anliegenApp.selectedDate.setHours(12);
      var datePickerDate = anliegenApp.selectedDate;
      anliegenApp.selectedDate = new Date(datePickerDate.getTime());
      console.log(anliegenApp.selectedDate);
      if(anliegenApp.selectedItems != null){
        if (anliegenApp.selectedItems.constructor === Array){
          anliegenApp.set(anliegenApp.selectedItems[0].datum, anliegenApp.selectedDate);  
        }else {
          anliegenApp.set(anliegenApp.selectedItems.datum, anliegenApp.selectedDate);
        }
        $('#editDate').val(anliegenApp.selectedDate);
        // anliegenApp.selectedItems[0].datum = anliegenApp.selectedDate;
      }else {
        anliegenApp.addAnliegenDatum = anliegenApp.selectedDate;
        // $('#addDate').val(anliegenApp.formatdate2(anliegenApp.addAnliegenDatum));
      }
    }


    document.addEventListener('myEvent', function() {
      anliegenApp.firstStart = true;
        
      console.log('my Event triggered');
      //load dataTable functions
        anliegenApp.dataSource = {
        get: function(sort, page, pageSize){
            return new Promise(function(resolve, reject){
                // console.log('getting data ' +anliegenApp.selectedAktion);
                var sortProperty;
                if (sort.property == 'id'){
                  sortProperty = 'objectId';
                }else {
                  sortProperty = sort.property;
                }
                console.log(sortProperty);
                $.post( "getdata.php", {'m': 'dictionary', 
                    'sortCol': sortProperty, 'sortDirec': sort.direction, 'page': page, 'pageSize': pageSize}, function( result) {
                  console.log(result);
                  var returnedData = jQuery.makeArray( result );
                  // var obj;
                  // for ( var i = 0, l = returnedData.length; i < l; i++ ) {
                  //     obj = returnedData[ i ];
                  //     console.log(obj);
                  //     return obj;
                  // }
                  $.each( returnedData, function( key, value ) {
                    console.log(value);
                    return value;
                  });
                  
                  resolve(returnedData);  
                }, "json");
            });
                  
          },
              
        set: function(id, property, value){
          return new Promise(function(resolve, reject){
            console.info("a save was triggered", arguments);
            resolve(arguments);
          });
        },
        length:anliegenApp.anliegenCount
      };
    });
    });

    
        
      
    

    
 

</script>
<?php
  }else{
    header('Location: login.php');
  }
?>
</body>
<!-- </html> -->