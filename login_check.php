<?php
include 'config.php';

use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseUser;
use Parse\ParseException;
use Parse\ParseSessionStorage;

// session_start();
//Set session storage
ParseClient::setStorage( new ParseSessionStorage() );

$username = stripslashes($_POST['user']); 
$pass = stripslashes($_POST['pass']);
$result = array();

try {
  $user = ParseUser::logIn($username, $pass);
  // echo "Email lautet: " . $user->get("email") . ".<br>";
  if (isSuperAdmin($username)){
  	$_SESSION['level'] = "admin";	
  }else {
  	$_SESSION['level'] = "user";
  }

  $result['level'] = $_SESSION['level'];
  $result['error'] = null;
  $result['user'] = json_encode($user);
  
  echo json_encode($result);
  // echo "currentUser: " . print_r($user);
} catch (ParseException $error) {
  $result['error'] = json_encode($error);
  $result['user'] = null;
  echo json_encode($result);
}

function isSuperadmin($username){
	global $superAdmins;
	if (in_array($username, $superAdmins)){
		return true;
	}else {
		return false;
	}
}
?>