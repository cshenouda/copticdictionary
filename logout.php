<?php
include 'config.php';

use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseUser;
use Parse\ParseException;
use Parse\ParseSessionStorage;

// session_start();
//Set session storage
ParseClient::setStorage( new ParseSessionStorage() );

if (isset($_SESSION['parseData']['user'])) {
	try {
  		// $user = ParseUser::getCurrentUser();
  		ParseUser::logOut();
  		echo "true";
	} catch (ParseException $error) {
		echo "false";
	}

}else {
	echo "false";
}

?>