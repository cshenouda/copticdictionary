<?php
// require 'app/vendor/autoload.php';
include 'config.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseException;
use Parse\ParseACL;

if (isset($_POST['m'])){
	$method = stripslashes($_POST['m']);	
}

if (isset($_GET['m']) && $_GET['m'] == 'standby') {
	try{
		if (isset($_GET['j'])) {
			$job = stripslashes($_GET['j']);
			$query = new ParseQuery("Newsletter_Job");
			$query->equalTo("objectId", $job);
			$matchingJob = $query->first(true);

			//set job mailOpened=true
			if ($matchingJob != null && !empty($matchingJob)){
				$matchingJob->set('mailOpened', true);
				$matchingJob->save(true);
			}

			//send back the 1x1 pixel
			header('Content-Type: image/jpeg');
      		echo file_get_contents('img/1x1.png');
		}

	}catch (Exception $e){
		$error['error'] = $e->getMessage();
	 	echo json_encode($error);
	}
}

if (isset($method) && $method == 'confirmAbo') {
	try{
		if (isset($_POST['user'])){
			$user = $_POST['user'];
		}

		$newsletterUser = new ParseObject("Newsletter_User", $user);
		$newsletterUser->fetch();
		$confirmedAbos = array();
		
		if ($newsletterUser != null){
			//Get user's unconfirmed Abos
			$query = new ParseQuery("Newsletter_Abo");
			$query->equalTo('subscribeRequested', true);
			$query->includeKey('anliegen');
			$query->equalTo('newsletterUser', $newsletterUser);
			$newsletterAbos = $query->find(true);
			
			// print_r($newsletterAbos);
			
			for ($i=0;$i<count($newsletterAbos);$i++){
				$abo = $newsletterAbos[$i];

				$abo->set('confirmed', true);
				$abo->set('subscribeRequested', false);
				$abo->save(true);

				$confirmedAbos[$i] = makeAnliegenArray($abo->get('anliegen'));
				$confirmedAbos[$i]['user'] = getAnrede($newsletterUser->get('firstName'), $newsletterUser->get('lastName'));
			}
		}

		echo json_encode($confirmedAbos);

	} catch (Exception $e){
	   echo $e->getMessage();
	}
}else if (isset($method) && $method == 'getAbos') {
	try{
		if (isset($_POST['user'])){
			$user = $_POST['user'];
		}
		
		$newsletterUser = new ParseObject("Newsletter_User", $user);
		$newsletterUser->fetch();
		$confirmedAbos = array();
		
		if ($newsletterUser != null){
			//Get user's confirmed Abos
			$query = new ParseQuery("Newsletter_Abo");
			$query->equalTo('confirmed', true);
			$query->includeKey('anliegen');
			$query->equalTo('newsletterUser', $newsletterUser);
			$newsletterAbos = $query->find(true);

			//Get user's subscribeRequested=true Abos
			$query = new ParseQuery("Newsletter_Abo");
			$query->equalTo('subscribeRequested', true);
			$query->includeKey('anliegen');
			$query->equalTo('newsletterUser', $newsletterUser);
			$subscribeRequestedAbos = $query->find(true);

			//Add confirmed and subscribeRequested Abos
			$confirmedAnliegen = array();
			foreach ($newsletterAbos as $abo){
				$confirmedAnliegen[] = $abo->get('anliegen')->getObjectId();
			}
			foreach ($subscribeRequestedAbos as $abo){
				$confirmedAnliegen[] = $abo->get('anliegen')->getObjectId();
			}
			
			//Get Anliegen that the user is not subscribed to
			$query = new ParseQuery("Anliegen");
			$query->notContainedIn('objectId', $confirmedAnliegen);
			$notSubscribedAnliegen = $query->find(false);

			$newsletterSettings = array();
			$newsletterSettings['user'] = makeUserArray($newsletterUser);
			for ($i=0; $i < count($newsletterAbos); $i++){
				$newsletterSettings['abos'][$i] = makeAnliegenArray($newsletterAbos[$i]->get('anliegen'), $newsletterUser->getObjectId());
			}

			for ($i=0; $i < count($notSubscribedAnliegen); $i++){
				$newsletterSettings['notSubscribed'][$i] = makeAnliegenArray($notSubscribedAnliegen[$i]);
			}
			echo json_encode($newsletterSettings);
		}else {
			return "Benutzer existiert nicht!";
		}


	} catch (Exception $e){
	   echo $e->getMessage();
	}
}else if (isset($method) && $method == 'addAbos') {
	try{
		if (isset($_POST['user'])) {
			$user = stripslashes($_POST['user']);
		}

		if (!empty($_POST['aktion'])) {
			$aktionen = array();
			foreach($_POST['aktion']['aktion'] as $aktion) {
				$aktionen[] = $aktion;
			}
		}

		$defaultACL = new ParseACL();
		$defaultACL->setPublicReadAccess(true);
		$defaultACL->setPublicWriteAccess(false);
		

		// //Get user's confirmed Abos
		// $query = new ParseQuery("Newsletter_Abo");
		// $query->equalTo('confirmed', true);
		// $query->includeKey('anliegen');
		// $query->equalTo('newsletterUser', $newsletterUser);
		// $newsletterAbos = $query->find(true);

		// $confirmedAnliegen = array();
		// foreach ($newsletterAbos as $abo){
		// 	$confirmedAnliegen[] = $abo->get('anliegen')->getObjectId();
		// }

		// //Create newsletterAbos
		// foreach ($aktionen as $aktionId) {
		// 	//Check if abo already exists (can happen if it wasn't confirmed yet)
		// 	if (!in_array($aktionId, $confirmedAnliegen)){
		// 		$aktion = ParseObject::create("Anliegen", $aktionId, false);
		// 		$aktion->fetch(true);
		// 		$newsletterAbo = new ParseObject("Newsletter_Abo");
		// 		$newsletterAbo->set("newsletterUser", $newsletteruser);
		// 		$newsletterAbo->set("anliegen", $aktion);
		// 		$newsletterAbo->set("intervall", 1);
		// 		$newsletterAbo->set("confirmed", false);
		// 		$newsletterAbo->set("unsubscribeRequested", false);
		// 		$newsletterAbo->set("subscribeRequested", true);
		// 		$newsletterAbo->setACL($defaultACL);
		// 		$newsletterAbo->save(true);
		// 	}
		// }


		//Create newsletterUser
		$newsletteruser = new ParseObject("Newsletter_User", $user);
		$newsletteruser->fetch();

		//Create newsletterAbos
		foreach ($aktionen as $aktionId) {
			$aktion = ParseObject::create("Anliegen", $aktionId, false);
			$aktion->fetch(true);
			$newsletterAbo = new ParseObject("Newsletter_Abo");
			$newsletterAbo->set("newsletterUser", $newsletteruser);
			$newsletterAbo->set("anliegen", $aktion);
			$newsletterAbo->set("intervall", 1);
			$newsletterAbo->set("confirmed", false);
			$newsletterAbo->set("unsubscribeRequested", false);
			$newsletterAbo->set("subscribeRequested", true);
			$newsletterAbo->setACL($defaultACL);
			$newsletterAbo->save(true);
		}

		//Add job for sending confirmation mail
		$newsletterJob = new ParseObject("Newsletter_Job");
		$newsletterJob->set('action', "sendMail");
		$newsletterJob->set('newsletterUser', $newsletteruser);

		$newsletteruser->fetch(true);
		$newsletterJob->set('content', getNewsletterMailConfirmationText(getAnrede($newsletteruser->get('firstName'), 
					$newsletteruser->get('lastName')), getConfirmationLink($newsletteruser->getObjectId(), true)));
		$newsletterJob->set('title', "Anmeldung zum Deutschland-betet-Newsletter");
		$newsletterJob->set('done', false);
		$newsletterJob->set('pending', false);
		//we're not setting schedule date, because we want the email to be sent immediately
		$newsletterJob->setACL($defaultACL);
		$newsletterJob->save(true);


		echo json_encode($newsletteruser);
	}catch (Exception $e){
		$error['error'] = $e->getMessage();
	 	echo json_encode($error);
	}
}else if (isset($method) &&$method == 'editNewsletterUser') {
	try{
		if (isset($_POST['firstName'])) {
			$firstName = stripslashes($_POST['firstName']);
		}
		if (isset($_POST['lastName'])) {
			$lastName = stripslashes($_POST['lastName']);
		}
		if (isset($_POST['email'])) {
			$email = stripslashes($_POST['email']);
		}
		if (isset($_POST['user'])) {
			$user = stripslashes($_POST['user']);
		}

		$defaultACL = new ParseACL();
		$defaultACL->setPublicReadAccess(true);
		$defaultACL->setPublicWriteAccess(false);
		
		//Create newsletterUser
		$newsletteruser = new ParseObject("Newsletter_User", $user);
		$newsletteruser->set('firstName', $firstName);
		$newsletteruser->set('lastName', $lastName);
		$newsletteruser->set('email', $email);
		$newsletteruser->setACL($defaultACL);
		$newsletteruser->save(true);

		//Add job for sending confirmation mail
		$newsletterJob = new ParseObject("Newsletter_Job");
		$newsletterJob->set('action', "sendMail");
		$newsletterJob->set('newsletterUser', $newsletteruser);

		$newsletteruser->fetch(true);
		$newsletterJob->set('content', getEditDataMailConfirmationText(getAnrede($firstName, $lastName), getEditDataConfirmationLink($newsletteruser->getObjectId())));
		$newsletterJob->set('title', "Änderung Ihrer Newsletter-Einstellungen");
		$newsletterJob->set('done', false);
		$newsletterJob->set('pending', false);
		//we're not setting schedule date, because we want the email to be sent immediately
		$newsletterJob->setACL($defaultACL);
		$newsletterJob->save(true);


		echo json_encode($newsletteruser);
	}catch (Exception $e){
		$error['error'] = $e->getMessage();
	 	echo json_encode($error);
	}
}else if (isset($method) && $method == 'removeAbo') {
	try{
		if (isset($_POST['user'])) {
			$user = stripslashes($_POST['user']);
		}

		if (isset($_POST['aktion'])) {
			$aktionId = $_POST['aktion'];
		}

		$defaultACL = new ParseACL();
		$defaultACL->setPublicReadAccess(true);
		$defaultACL->setPublicWriteAccess(false);
		
		//Create newsletterUser
		$newsletteruser = new ParseObject("Newsletter_User", $user);
		$newsletteruser->fetch();

		//Remove newsletterAbo
		$aktion = ParseObject::create("Anliegen", $aktionId, false);
		$aktion->fetch(true);

		$query = new ParseQuery("Newsletter_Abo");
		$query->equalTo("newsletterUser", $newsletteruser);
		$query->equalTo("anliegen", $aktion);
		$abo = $query->first(true);

		if ($abo != null){
			$abo->get("anliegen")->fetch(true);
			$return['anliegen'] = $abo->get("anliegen")->get("titel");

			$abo->destroy(true);	
			$abo->save(true);

			// //Add job for sending confirmation mail
			// $newsletterJob = new ParseObject("Newsletter_Job");
			// $newsletterJob->set('action', "sendMail");
			// $newsletterJob->set('newsletterUser', $newsletteruser);

			// $newsletteruser->fetch(true);
			// $newsletterJob->set('content', getNewsletterMailConfirmationText(getAnrede($firstName, $lastName), getConfirmationLink($newsletteruser->getObjectId(), true)));
			// $newsletterJob->set('title', "Anmeldung zum Deutschland-betet-Newsletter");
			// $newsletterJob->set('done', false);
			// $newsletterJob->set('pending', false);
			// //we're not setting schedule date, because we want the email to be sent immediately
			// $newsletterJob->setACL($defaultACL);
			// $newsletterJob->save(true);

			echo json_encode($return);
		}
	}catch (Exception $e){
		$error['error'] = $e->getMessage();
	 	echo json_encode($error);
	}
}else if (isset($method) && $method == 'viewNewsletter') {
	try{
		if (isset($_POST['user'])) {
			$user = stripslashes($_POST['user']);
			$query = new ParseQuery("Newsletter_User");
			$query->equalTo("objectId", $user);
			$newsletterUser = $query->first(true);
		}

		if (isset($_POST['job'])) {
			$job = stripslashes($_POST['job']);
		}

		if ($job != null && $newsletterUser != null){ //user isn't really necessary
			$query = new ParseQuery("Newsletter_Job");
			$query->equalTo("objectId", $job);
			$query->equalTo("newsletterUser", $newsletterUser);
			$matchingJob = $query->first(true);

			if ($matchingJob != null && !empty($matchingJob)){
				//found a matching job
				$returnJob = array();
				$returnJob['id'] = $matchingJob->getObjectId();
				$returnJob['content'] = $matchingJob->get("content");
				$returnJob['title'] = $matchingJob->get("title");
				$returnJob['email'] = $newsletterUser->get("email");
				$returnJob['anrede'] = getAnrede($newsletterUser->get('firstName'), 
					$newsletterUser->get('lastName'));
				
				echo json_encode(($returnJob));
			}else {
				return null;
			}
		}else {
			return null;
		}
	}catch (Exception $e){
		$error['error'] = $e->getMessage();
	 	echo json_encode($error);
	}
	//track mail open with 1x1 tracking pixel
}

function makeUserArray($newsletterUser){
	$newsletterSettings['anrede'] = getAnrede($newsletterUser->get('firstName'), $newsletterUser->get('lastName'));
	$newsletterSettings['firstName'] = $newsletterUser->get('firstName');
	$newsletterSettings['lastName'] = $newsletterUser->get('lastName');
	$newsletterSettings['email'] = $newsletterUser->get('email');
	return $newsletterSettings;
}
function makeAnliegenArray($abo, $userId = null){
	$confirmedAbos['anliegen']['id'] = $abo->getObjectId();
	$confirmedAbos['anliegen']['titel'] = $abo->get('titel');
	$confirmedAbos['anliegen']['bild'] = $abo->get("bild");
	if ($userId != null){
		$confirmedAbos['anliegen']['unsubscribeLink'] = getUnsubscribeLink($userId, false, $abo->getObjectId());	
	}
	if ($confirmedAbos['anliegen']['bild'] != null) {
		$confirmedAbos['anliegen']['bild_url'] = $confirmedAbos['anliegen']['bild']->getURL();	
	}
	return $confirmedAbos;
}

function getAnrede($firstName, $lastName){
	$space = " ";
	$default = "Beter";

	if ($firstName == null){
		$firstName = "";
		$space = "";
	}

	if ($lastName == null){
		$lastName = "";
		$space = "";
	}

	if ($firstName == null && $lastName == null){
		return $default;
	}else {
		return $firstName.$space.$lastName;
	}
}

function getConfirmationLink($userId, $subscribe=true){
	return "http://deutschlandbetet.de/newsletter-bestaetigung?u=".$userId."&subscribe=".+$subscribe;
}

function getUnsubscribeLink($userId, $subscribe=false, $aktionId=null){
	if ($aktionId == null){
		return "http://deutschlandbetet.de/newsletter-einstellungen?u=".$userId;
	}else {
		return "http://deutschlandbetet.de/newsletter-bestaetigung?u=".$userId."&subscribe=".+$subscribe."&aktion=".$aktionId;	
	}
	
}

function getMailLink($userId, $jobId){
	return "http://deutschlandbetet.de/newsletter-mail?u=".$userId."&j=".$jobId;
}

function getTrackingLink($jobId){
	return "http://deutschlandbetet.de/admin/newsletter_helper.php?m=standby&j=".$jobId;
}


function getEditDataConfirmationLink($userId){
	return "http://deutschlandbetet.de/newsletter-einstellungen?u=".$userId;
}

function sendMail($job){
	$mail = new PHPMailer;

	//$mail->SMTPDebug = 3;                               // Enable verbose debug output

	$mail->SMTPOptions = array(
	    'ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true
	    )
	);

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->CharSet = 'utf-8'; 
	$mail->Host = 'mail.deutschlandbetet.de';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'newsletter@deutschlandbetet.de';                 // SMTP username
	$mail->Password = 'Tkmx04$1';                           // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                    // TCP port to connect to

	$mail->setFrom('newsletter@deutschlandbetet.de', 'Deutschland betet');
	$mail->addAddress($job->get('newsletterUser')->get('email'), 
		getAnrede($job->get('newsletterUser')->get('firstName'), $job->get('newsletterUser')->get('lastName')));     // Add a recipient
	$mail->addReplyTo('info@deutschlandbetet.de', 'Deutschland betet');
	
	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = $job->get('title');
	$mail->Body    = $job->get('content');
	$mail->AltBody = $job->get('content');

	if(!$mail->send()) {
	    return false;
	} else {
	    return true;
	}
}
?>