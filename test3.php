<?php 
include 'getdata.php';
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseUser;
use Parse\ParseException;
use Parse\ParseSessionStorage;
use Parse\ParseACL;

echo getAnliegen3(null, new DateTime(), new DateTime(), null, null, null, null, false, false);

function getAnliegen3($mAnliegenId, $mDatumStart, $mDatumEnd, $mSortCol, $mSortDirec, $mPage, $mPageSize, $mMaster, $mIsWidget=false){
  try{
   if ($mMaster == 'true'){
    $mMaster = true;
   }else {
    $mMaster = false;
   }   
   
   $query = new ParseQuery("Gebet");
   if (isset($mAnliegenId) && $mAnliegenId != null){
    $query->equalTo("anliegenId", $mAnliegenId);  
   }

   if (isset($mDatumStart) && $mDatumStart != null) {
    $start = clone $mDatumStart;
    $start->setTime(0,0,0);
    $end = clone $mDatumEnd;
    $end->setTime(23,59,59);
    $query->greaterThan("datum", $start);
    $query->lessThan("datum", $end);
    echo $start->format('c');
    echo "\n";
    echo $end->format('c');
   }

   $query->limit(1000);
   if (isset($mSortCol) && $mSortCol != null) {
    if ($mSortDirec == "asc"){
      $query->ascending($mSortCol);
    }else{
      $query->descending($mSortCol);
    }
    $query->skip(($mPage-1) * $mPageSize);
    $query->limit($mPageSize);
   }else {
    $query->descending('datum');
   }

     if ($mIsWidget == true){
        $query->limit(5);
     }

   $query->includeKey("anliegen");
   $results = $query->find($mMaster);
   $gebete = array();
   
   for ($i = 0; $i < count($results); $i++) {
     $gebete[$i]['id'] = $results[$i]->getObjectId();
     $gebete[$i]['titel'] = $results[$i]->get("titel");
     $gebete[$i]['text'] = $results[$i]->get("text");
     $gebete[$i]['kurztext'] = $results[$i]->get("kurztext");
     $gebete[$i]['datum'] = $results[$i]->get("datum")->format('c');
     $gebete[$i]['updatedAt'] = $results[$i]->getUpdatedAt()->format('c');
     
     // $anlQuery = new ParseQuery("Anliegen");
     // $anlQuery->equalTo("objectId", $results[$i]->get("anliegenId"));
     // $anl = $anlQuery->first(true);
     $gebete[$i]['anliegen'] = json_decode(getAktionen(null, $results[$i]->get("anliegenId"), 'true'), true);

     $gebete[$i]['bild'] = $results[$i]->get("bild");
     if ($gebete[$i]['bild'] != null){
      $gebete[$i]['bild_url'] = $gebete[$i]['bild']->getURL();  
     }else {
      $anliegen = $results[$i]->get("anliegen");
      if ($anliegen != null) {
        $anliegen->fetch($mMaster);
        $gebete[$i]['bild'] = $anliegen->get("bild");
        if ($gebete[$i]['bild'] != null){
          $gebete[$i]['bild_url'] = $gebete[$i]['bild']->getURL();  
        }
      }
     }
   }

   if ($mIsWidget == true){
    usort($gebete, "cmpAnliegenCreatedAt");
   }
   return json_encode($gebete);
   } catch (Exception $e){
      return $e->getMessage();
   }
}
?>