  <?php
    session_start();
    if (isset($_SESSION['parseData']['user'])) 
    {
  ?>
  <script src="polymer/bower_components/webcomponentsjs/webcomponents.js"></script>
  
  <!-- <link rel="import" href="polymer/bower_components/polymer/polymer.html">
  <link rel="import" href="polymer/bower_components/paper-button/paper-button.html">
  <link href="polymer/bower_components/paper-drawer-panel/paper-drawer-panel.html" rel="import">
  <link href="polymer/bower_components/paper-header-panel/paper-header-panel.html" rel="import">
  <link href="polymer/bower_components/paper-toolbar/paper-toolbar.html" rel="import">
  <link href="polymer/bower_components/paper-icon-button/paper-icon-button.html" rel="import">
  <link href="polymer/bower_components/paper-material/paper-material.html" rel="import">
  <link href="polymer/bower_components/paper-menu/paper-menu.html" rel="import"> 
  <link href="polymer/bower_components/paper-item/paper-item.html" rel="import">
  <link href="polymer/bower_components/paper-input/paper-input.html" rel="import"> -->
  <link href="polymer/bower_components/paper-input/paper-textarea.html" rel="import">
  <!-- <link href="polymer/bower_components/iron-icons/iron-icons.html" rel="import"> -->
  <link href="polymer/bower_components/paper-datatable/paper-datatable.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-card.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-column.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/datatable-icons.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-edit-dialog.html" rel="import">
  <link href="polymer/bower_components/paper-datatable/paper-datatable-styles.html" rel="import">
  <link rel="import" href="polymer/bower_components/paper-dialog/paper-dialog.html">
  <link rel="import" href="polymer/bower_components/paper-dialog-scrollable/paper-dialog-scrollable.html">
  <link rel="import" href="polymer/bower_components/neon-animation/neon-animations.html">
   <link href="polymer/bower_components/paper-toggle-button/paper-toggle-button.html" rel="import">
<style>
/*body{
  overflow:hidden;
}
paper-card{
  margin-bottom:20px;
  display:block;
}
paper-card {
  --paper-card-header-text: {
    font-family: 'Roboto', 'Noto', sans-serif;
    font-weight: normal;
    font-size: 20px;
  }
}*/


</style>
<body>
  
  <div id="main">
    <template is="dom-bind" id="aktionenApp">

      <!-- <paper-button on-tap="testClick">TEST</paper-button> -->
     <!--  <paper-card heading="Meine Aktionen">
          <paper-datatable id="aktionenTable" data="{{data}}" selectable multi-selection>
            <paper-datatable-column header="Titel" property="titel" type="String" editable dialog edit-icon sortable sorted>
              <template>
                  <paper-input value="{{value}}" no-label-float maxlength="40" char-counter></paper-input>
                </template>
            </paper-datatable-column>
            <paper-datatable-column header="Beschreibung" property="beschreibung" type="String" editable dialog edit-icon>
              <template>
                  <paper-input value="{{value}}" no-label-float maxlength="400" char-counter></paper-input>
                </template>
            </paper-datatable-column>
            <paper-datatable-column header="Bild" property="bild" type="String" sortable></paper-datatable-column>
          </paper-datatable>
       </paper-card> -->

<paper-dialog id="editDialogAktionen" with-backdrop style="max-width:500px;min-height:300px;">
  <template is="dom-repeat" items="{{selectedItems}}" filter="{{onlyFirst}}">
    <h2>Aktion #<span>{{item.id}}</span></h2>
      <paper-input value="{{item.titel}}" label="Titel"></paper-input>
      <paper-textarea value="{{item.beschreibung}}" label="Beschreibung"></paper-textarea>
      <label id="liveLabel">Live?</label><paper-toggle-button checked="{{item.public}}" on-change="liveToggled" id="liveToggle"></paper-toggle-button>
        <div class="passLabel">Passwortgeschützt?</script></div><paper-toggle-button on-change="passToggled" id="passToggle"></paper-toggle-button>
      <?php
        if ($_SESSION['level'] == "admin") {
      ?>
        <paper-input value="{{bild}}" label="Bild"></paper-input>
      <?php
        }
      ?>
    <div class="buttons">
      <paper-button dialog-confirm on-tap="confirmSave">Speichern</paper-button>
    </div>
  </template>
</paper-dialog>

<paper-dialog id="addDialogAktion" with-backdrop style="max-width:500px;min-height:300px;">
  <!-- <template is="dom-repeat"> -->
    <h2>Neue Aktion</span></h2>
      <paper-dialog-scrollable>
        <paper-input value="{{addAktionTitel}}" label="Titel"></paper-input>
        <paper-textarea value="{{addAktionBeschreibung}}" label="Beschreibung"></paper-textarea>
      </paper-dialog-scrollable>
    <div class="buttons">
      <paper-button dialog-confirm on-tap="confirmSave">Speichern</paper-button>
    </div>
  <!-- </template> -->
</paper-dialog>

<paper-dialog id="confirmationDialog" with-backdrop style="max-width:500px;min-height:300px;">
  <!-- <template is="dom-repeat" items="{{selectedItems}}" filter="{{onlyFirst}}"> -->
    <h2>Speichern</h2>
      <paper-dialog-scrollable>
        Sind Sie sicher, dass Sie diese Änderungen speichern wollen?<br>Änderungen sind nicht wiederherstellbar.
      </paper-dialog-scrollable>
    <div class="buttons">
      <paper-button dialog-confirm on-tap="saveAktion">Ja</paper-button>
      <paper-button dialog-dismiss>Nein</paper-button>
    </div>
  <!-- </template> -->
</paper-dialog>

<paper-dialog id="confirmationDeleteDialog" with-backdrop style="max-width:500px;min-height:300px;">
  <!-- <template is="dom-repeat" items="{{selectedItems}}" filter="{{onlyFirst}}"> -->
    <h2>Löschen</h2>
      <paper-dialog-scrollable>
        Sind Sie sicher, dass Sie dieses Objekt <span style="color:red">löschen</span> wollen?<br>Änderungen sind nicht wiederherstellbar.
      </paper-dialog-scrollable>
    <div class="buttons">
      <paper-button dialog-confirm on-tap="deleteAktion">Ja</paper-button>
      <paper-button dialog-dismiss>Nein</paper-button>
    </div>
  <!-- </template> -->
</paper-dialog>

<paper-dialog id="confirmationSecureDialog" with-backdrop style="max-width:500px;min-height:300px;">
  <!-- <template is="dom-repeat" items="{{selectedItems}}" filter="{{onlyFirst}}"> -->
    <h2>Passwort</h2>
      <paper-dialog-scrollable>
        Hiermit wird diese Aktion durch ein Passwort gesichert. Nur Personen, denen Sie dieses Passwort mitteilen, können die enthaltenen Gebetsanliegen sehen. <br><b>Sind Sie sicher, dass Sie diese Aktion durch ein Passwort sichern wollen?</b>
      </paper-dialog-scrollable>
    <div class="buttons">
      <paper-button dialog-confirm on-tap="secureAktion">Ja</paper-button>
      <paper-button dialog-dismiss>Nein</paper-button>
    </div>
  <!-- </template> -->
</paper-dialog>

<paper-dialog id="confirmationUnsecureDialog" with-backdrop style="max-width:500px;min-height:300px;">
  <!-- <template is="dom-repeat" items="{{selectedItems}}" filter="{{onlyFirst}}"> -->
    <h2>Passwort</h2>
      <paper-dialog-scrollable>
        Hiermit wird diese Aktion <span style="color:red">nicht mehr </span>durch ein Passwort gesichert. Wenn Sie die Aktion zu einem späteren Zeitpunkt erneut sichern möchten, werden Sie dafür ein <span style="color:red">neues Passwort</span> erhalten. Personen, die zuvor Zugang zu der Aktion hatten, erhalten <span style="color:red">nicht</span> automatisch wieder Zugang. Diese müssen das neue Passwort eingeben. <span style="color:red">WICHTIG: </span>Ihre Aktion wird nicht öffenlich sichtbar sein, um zu verhindern, dass eventuell sensible Informationen aus Ihren Anliegen öffentlich werden. Möchten Sie die Aktion doch live schalten, betätigen Sie den Live-Schalter im vorherigen Fenster. <br><b>Sind Sie sicher, dass Sie fortfahren möchten?</b>
      </paper-dialog-scrollable>
    <div class="buttons">
      <paper-button dialog-confirm on-tap="unsecureAktion">Ja</paper-button>
      <paper-button dialog-dismiss>Nein</paper-button>
    </div>
  <!-- </template> -->
</paper-dialog>

<paper-datatable-card id="datatableCardAktionen" header="Aktionen" page-size="10" data-source="{{dataSource}}" id-property="id" selected-ids="{{selectedIds}}" style="width:99%">
          <div toolbar-main>
            <!-- <paper-input value="{{searchTerm}}" on-input="retrieveResults" label="Search..." style="display:inline-block" no-label-float>
              <div prefix>
                <iron-icon icon="search"></iron-icon>
              </div>
            </paper-input> -->
            <paper-icon-button icon="cached" on-tap="retrieveResults"></paper-icon-button>
            <paper-icon-button icon="add" on-tap="addAktion"></paper-icon-button>
          </div>
          <div toolbar-select>
            <paper-icon-button icon="delete" on-tap="deleteSelected"></paper-icon-button>
          </div>
          <div toolbar-select-single>
            <paper-icon-button icon="datatable:editable" on-tap="editDialogForSelected"></paper-icon-button>
          </div>
          <paper-datatable id="datatableAktionen" style="width:99%" selectable multi-selection selected-items="{{selectedItems}}" on-row-tap="rowTapped">
            <div no-results>
              Lade oder es gibt keine weiteren Elemente...
            </div>
            <paper-datatable-column header="ID" property="id" type="String" sortable sorted>
            </paper-datatable-column>
            <paper-datatable-column header="Aktualisiert am" property="updatedAt" format-value="{{formatdate}}" type="date" style="min-width: 120px" sortable>
            <!-- <template>
              <div><span>[[formatdate(value.date)]]</span></div>
            </template> -->
            </paper-datatable-column>
            <paper-datatable-column header="Titel" property="titel" type="String" sortable style="min-width: 300px">
            </paper-datatable-column>
            <paper-datatable-column header="Beschreibung" property="beschreibung" type="String" style="min-width: 320px">
            </paper-datatable-column>
            <paper-datatable-column header="Bild" property="bild" sortable>
              <template>
                <div>
                  <img src="[[item.bild_url]]" style="width:32px;height:32px;border-radius:16px;vertical-align: middle;margin-right:4px;">
                </div>
              </template>
            </paper-datatable-column>
            <?php
              if ($_SESSION['level'] == "admin") {
            ?>
              <paper-datatable-column header="Passwort" property="password" type="String" sortable style="min-width: 200px">
              </paper-datatable-column>
            <?php
              }
            ?>
            <paper-datatable-column header="Öffentlich" format-value="{{formatpublic}}" property="public" type="String" sortable style="min-width: 100px">
            </paper-datatable-column>
          </paper-datatable>
        </paper-datatable-card>



       
        

    <div class="result">
      
    </div>
    </template>
  </div>
  
  <script>

  document.addEventListener('WebComponentsReady', function() {
   
    var aktionenApp = document.querySelector('#aktionenApp');
    var passToggleOriginal = false;
  
    aktionenApp.formatpublic = function (str){
      if (str == 'true'){
        return "Ja";
      }else{
        return "Nein";
      }
    }

    aktionenApp.formatdate = function(str) {
      console.log(str);
      return $.format.date(str, "dd.MM.yyyy");
    };

    aktionenApp.dataSource = {
      get: function(sort, page, pageSize){
          return new Promise(function(resolve, reject){
            $.post( "getdata.php", {'m': 'aktionen', 'anliegenId': null, 'master': 'true'}, function( result) {
              var returnedData = jQuery.makeArray( result );
              console.log("receiving Aktionen: " +returnedData);
              $.each( returnedData, function( key, value ) {
                console.log(value);
                return value;
              });
             
              resolve(returnedData);  
              }, "json");
            });
                
        },
           
      set: function(id, property, value){
        return new Promise(function(resolve, reject){
          console.info("a save was triggered", arguments);
          resolve(arguments);
        });
      },
      length:1
    };
  
     aktionenApp.passToggled = function (e) {
      var toggle = document.getElementById('passToggle');
      if (toggle.checked){
        this.$.confirmationSecureDialog.open(); 
      }else{
        this.$.confirmationUnsecureDialog.open();
      }
    }

    aktionenApp.secureAktion = function (e){
       $.post( "adddata.php", {'m': 'setAktionSecured', 'anliegenId':  aktionenApp.selectedIds[0], 'setSecured': true}, function( result) {
          console.log(result);
          aktionenApp.retrieveResults();
          $('#editDialogAktionen').toggle();
          aktionenApp.setToggleStates();
        }, "json"); 
    }

    aktionenApp.unsecureAktion = function (e){
      $.post( "adddata.php", {'m': 'setAktionSecured', 'anliegenId':  aktionenApp.selectedIds[0], 'setSecured': false}, function( result) {
          console.log(result);
          aktionenApp.retrieveResults();
          $('#editDialogAktionen').toggle();
          aktionenApp.setToggleStates();
        }, "json"); 
    }

    aktionenApp.capitalize = function(str){
      return str.toString().split(' ').map(function(str){
        return str.substr(0,1).toUpperCase() + str.substr(1, str.length - 1);
      }).join(' ');
    };

    aktionenApp.deleteSelected = function(){
      this.$.confirmationDeleteDialog.open();
    };

    aktionenApp.deleteAktion = function(e){
      var deleteAktionId = aktionenApp.selectedIds[0];
      console.log('delete '+deleteAktionId);
      $.post( "deletedata.php", {'m': 'deleteAktion', 'anliegenId': deleteAktionId}, function( result) {
          console.log(result);
          aktionenApp.retrieveResults();
        }, "json"); 
    }

    function writePassLabel (){
      var pass = aktionenApp.selectedItems[0].password;
      if (pass != null && pass.length > 0){
        return "Durch Passwort geschützt?" + " <b>JA: " + pass + "</b>";
      }else {
        return "Durch Passwort geschützt?" + " <b>Nein</b>";
      }
      console.log("asldf");
    }

    aktionenApp.setToggleStates = function (){
      console.log("setToggles");
      var pass = aktionenApp.selectedItems[0].password;
      var passToggle = $('#passToggle');
      // var liveToggle = $('#liveToggle');
      var passLabel = $('.passLabel');
      
      if (pass != null && pass.length > 0){
        passToggle.attr('checked', true);
        passLabel.html("Passwortgeschützt?" + " <b>JA: " + pass + "</b>");
        console.log(passLabel.html());
      }else {
        passToggle.attr('checked', false);
        passLabel.html("Passwortgeschützt?" + " <b>Nein</b>");
        console.log(passLabel.html());
      }
    }

    aktionenApp.editDialogForSelected = function(){
      var editDialog = this.$.editDialogAktionen;
      editDialog.open();
      editDialog.addEventListener("iron-overlay-opened", function () {
        editDialog.removeEventListener("iron-overlay-opened", aktionenApp.setToggleStates(), false);
      });
      
    };

    aktionenApp.onlyFirst = function(item, index){
      // if(typeof index === 'undefined'){
      //   alert('Please see issue #34');
      // }
      // if(index == 0){
        return true;
      // }
    };

    aktionenApp.rowTapped = function(ev){
      if(aktionenApp.isDebouncerActive('dblclick'+ev.detail.item.id)){
        //double click
        aktionenApp.$.datatableCardAktionen.deselectAll();
        aktionenApp.$.datatableCardAktionen.select(ev.detail.item.id);
        aktionenApp.editDialogForSelected();
        ev.preventDefault();
        aktionenApp.cancelDebouncer('dblclick');
      }else{
        aktionenApp.debounce('dblclick'+ev.detail.item.id, function(){}, 300);
      }
    };

    aktionenApp.retrieveResults = function(ev){
      aktionenApp.$.datatableCardAktionen.retrieveVisibleData();
    };

    aktionenApp.confirmSave = function (e) {
      this.$.confirmationDialog.open();
    }

    aktionenApp.saveAktion = function (e) {
      console.log("save " +aktionenApp.selectedIds[0]);
      console.log(aktionenApp.selectedItems);
      if(aktionenApp.selectedItems != null){
        console.log("save " +aktionenApp.selectedIds[0]);
        var editAktion = aktionenApp.selectedItems[0];
        $.post( "adddata.php", {'m': 'addAktion', 'anliegenId': aktionenApp.selectedIds[0], 'titel': editAktion.titel, 'beschreibung': editAktion.beschreibung, 'bild': aktionenApp.bild, 'isLive': editAktion.public, 'password': editAktion.password}, function( result) {
          console.log(result);
          aktionenApp.retrieveResults();
        }, "json");  
      }else {
        console.log("saving new Aktion " +aktionenApp.addAnliegenTitel);
        $.post( "adddata.php", {'m': 'addAktion', 'titel': aktionenApp.addAktionTitel, 'beschreibung': aktionenApp.addAktionBeschreibung, 'password': aktionenApp.password}, function( result) {
          console.log(result);
          aktionenApp.retrieveResults();
        }, "json");
      }
      
    }

    aktionenApp.addAktion = function(e) {
      aktionenApp.selectedItems = null;
      aktionenApp.$.addDialogAktion.open();
    }

    
  });

  // document.addEventListener('WebComponentsReady', function() {
  //   var data = aktionenApp.$.datatableAktionen.data;
  //   console.log(data);
  //      if (data == null || jQuery.isEmptyObject(data)){
  //         console.log("WebComponentsReady, reloading");
  //         aktionenApp.retrieveResults();
  //      }
  //   });
</script>
<?php
  }else{
    header('Location: login.php');
  }
?>

</body>
<!-- </html> -->