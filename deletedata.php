<?php
include 'config.php';

use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseUser;
use Parse\ParseException;
use Parse\ParseSessionStorage;

$method = stripslashes($_POST['m']);
$user = $_SESSION['parseData']['user'];

if ($method == 'deleteAktion') {
	try{
		if (isset($_POST['anliegenId'])) {
			$anliegenId = stripslashes($_POST['anliegenId']);
		}
		
		$query = new ParseQuery("Anliegen");
		$query->equalTo("objectId", $anliegenId);
		$anliegen = $query->first(true);
		
		$anliegen->destroy(true);
		$anliegen->save(true);

		echo json_encode($anliegen);
	} catch (Exception $e){
	   echo $e->getMessage();
	}
}else if ($method == 'deleteDictEntry') {
	try{
		if (isset($_POST['id'])) {
			$idArray = $_POST['id'];
		}
		
		for ($i = 0; $i < count($idArray); $i++){
			$gebetId = $idArray[$i];
			$query = new ParseQuery("Dict");
			$query->equalTo("objectId", $gebetId);
			$gebet = $query->first(true);
			
			$gebet->destroy(true);
			$gebet->save(true);

			echo json_encode($gebet);
		}
	} catch (Exception $e){
	   echo $e->getMessage();
	}
}
?>