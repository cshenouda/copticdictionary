<?php
include ('config.php');

use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseUser;
use Parse\ParseException;
use Parse\ParseSessionStorage;
use Parse\ParseACL;

if (isset($_POST['m'])){
	$method = stripslashes($_POST['m']);
}

if (isset($_SESSION['parseData']['user'])){
	$user = $_SESSION['parseData']['user'];
}

if (isset($_SESSION['parseData']['user'])){
	try{
		$query = new ParseQuery("Anbieter");
		$query->equalTo("user", $user);
		$anbieter = $query->first(true);
	} catch (Exception $e){

	}
}

if (isset($_POST['anbieterId'])){
	try{
		$query = new ParseQuery("Anbieter");
		$query->equalTo("objectId", $_POST['anbieterId']);
		$anbieter = $query->first(true);
	} catch (Exception $e){

	}
}

if (isset($method)){
	if ($method == 'anbieter' && isset($anbieter)) {
		echo getAnbieter($anbieter, 'true');
	}else if ($method == 'allAnbieter') {
		if (isset ($_POST['master'])) {
	 		$master = $_POST['master'];
	 	}else {
	 		$master = 'true';
	 	}
		echo getAnbieter(null, $master);
	}else if ($method == 'aktionen') {
		if (isset($anbieter)){	
		}else {
			$anbieter = null;
		}

		if (isset ($_POST['anbieterId'])) {
			$anbieterId = $_POST['anbieterId'];
			$anbieter = new ParseObject("Anbieter", $anbieterId);
		}
		
		if (isset ($_POST['master'])) {
	 		$master = $_POST['master'];
	 	}else {
	 		$master = 'true';
	 	}

	 	if (isset ($_POST['anliegenId'])) {
	 		$anliegenId = $_POST['anliegenId'];
	 	}else {
	 		$anliegenId = null;
	 	}
	 	echo getAktionen($anbieter, $anliegenId, $master);
}else if ($method == 'anliegenCount') {
	if (isset($_POST['anliegenId'])) {
		$anliegenId = stripslashes($_POST['anliegenId']);
	}else {
		$anliegenId = null;
	}
	echo getAnliegenCount($anliegenId);
}else if ($method == 'dictionary') {
	
	//sorting and paging for the meineAnliegen.html page; paper-datatable
	if (isset($_POST['sortCol'])) {
		$sortCol = stripslashes($_POST['sortCol']);
	}else {
		$sortCol = null;
	}

	if (isset($_POST['sortDirec'])) {
		$sortDirec = stripslashes($_POST['sortDirec']);
	}else {
		$sortDirec = null;
	}

	if (isset($_POST['page'])) {
		$page = stripslashes($_POST['page']);
	}else {
		$page = null;
	}

	if (isset($_POST['pageSize'])) {
		$pageSize = stripslashes($_POST['pageSize']);
	}else {
		$pageSize = null;
	}
	
	if (isset($_POST['master'])) {
		$master = stripslashes($_POST['master']);
	}else {
		$master = 'false';
	}

	echo getDictionary($sortCol, $sortDirec, $page, $pageSize, $master);
}else if ($method == 'sonntage'){
	if (isset($_POST['anliegenId'])) {
		$anliegenId = stripslashes($_POST['anliegenId']);
	}
	echo getSonntage($anliegenId);
}else if ($method == 'allRes'){
	echo getAllRes();
}else if ($method == 'secureAktionen'){
	echo getSecureAktionen();
}
}

function getSecureAktionen(){
	try{

		$mMaster = true;
		 
		 $query = new ParseQuery("Anliegen");
		 $query->exists("password");
		 $results = $query->find($mMaster);
		 $anliegen = array();
		 for ($i = 0; $i < count($results); $i++) {
		 	$pass = $results[$i]->get("password");
		 	if (count($pass) > 3){

				 
		 		 $anliegen[$i]['id'] = $results[$i]->getObjectId();
			 	 // echo $anliegen[$i]['id'];
				 $anliegen[$i]['titel'] = $results[$i]->get("titel");
				 $anliegen[$i]['beschreibung'] = $results[$i]->get("beschreibung");
				 $anliegen[$i]['updatedAt'] = $results[$i]->getUpdatedAt()->format('c');
				 $anliegen[$i]['createdAt'] = $results[$i]->getCreatedAt()->format('c');
				 $anliegen[$i]['bild'] = $results[$i]->get("bild");
				 $anliegen[$i]['password'] = md5($results[$i]->get("password"));
				 if ($anliegen[$i]['bild'] != null) {
					$anliegen[$i]['bild_url'] = $anliegen[$i]['bild']->getURL();	
				 }
				 $isPublic = isAktionLive($anliegen[$i]['id']);
				 if ($isPublic == true) {
				 	$anliegen[$i]['public'] = "Ja";
				 }else {
				 	$anliegen[$i]['public'] = "Nein";
				 }
		 	}
		 	
			 
		 }
		 return json_encode($anliegen);
	 } catch (Exception $e){
	    return $e->getMessage();
	 }
}


function getAnbieter($mAnbieter, $mMaster){
	try{

		if ($mAnbieter != null) {
			$anb = array();
			$anb['id'] = $mAnbieter->getObjectId();
			$anb['name'] = $mAnbieter->get('name');
			$anb['spende_text'] = $mAnbieter->get('spende_text');
			// $anb['user'] = $mAnbieter->get('user');
			$anb['beschreibung'] = $mAnbieter->get('beschreibung');
			$anb['adresse'] = $mAnbieter->get('adresse');
			$anb['emailAdresse'] = $mAnbieter->get('emailAdresse');
			$anb['www'] = $mAnbieter->get('www');
			$anb['events'] = $mAnbieter->get('events');
			$anb['tel'] = $mAnbieter->get('tel');
			$anb['spende_www'] = $mAnbieter->get('spende_www');
			$bild = $mAnbieter->get('bild');
			
			if ($bild != null){
				// $anb['bild'] = new ParseFile();	
				$anb['bild_url'] = $bild->getURL();	
			}

			return json_encode($anb);
		}else {
			if ($mMaster == 'true'){
			 	$mMaster = true;
			 }else {
			 	$mMaster = false;
			 }
			
			 $query = new ParseQuery("Anbieter");
			 $query->ascending("name");
			 $results = $query->find($mMaster);
			 $anb = array();
			
			 for ($i = 0; $i < count($results); $i++) {
			 	$anb[$i]['id'] = $results[$i]->getObjectId();
				$anb[$i]['name'] = $results[$i]->get('name');
				$anb[$i]['spende_text'] = $results[$i]->get('spende_text');
				// $anb['user'] = $mAnbieter->get('user');
				$anb[$i]['beschreibung'] = $results[$i]->get('beschreibung');
				$anb[$i]['adresse'] = $results[$i]->get('adresse');
				$anb[$i]['emailAdresse'] = $results[$i]->get('emailAdresse');
				$anb[$i]['www'] = $results[$i]->get('www');
				$anb[$i]['events'] = $results[$i]->get('events');
				$anb[$i]['tel'] = $results[$i]->get('tel');
				$anb[$i]['spende_www'] = $results[$i]->get('spende_www');
				$bild = $results[$i]->get('bild');
				if ($bild != null){
					$anb[$i]['bild_url'] = $bild->getURL();	
				}
			 }
			
			return json_encode($anb);
		}
		
	} catch (Exception $e){
	    return $e->getMessage();
	}
}

function getAktionen($mAnbieter, $mAnliegenId, $mMaster) {

	try{

	if ($mMaster == 'true'){
	 	$mMaster = true;
	 }else {
	 	$mMaster = false;
	 }
	 
	 $query = new ParseQuery("Anliegen");
	 if ($mAnbieter != null){
	 	$query->equalTo("anbieter", $mAnbieter);	
	 }

	 if ($mAnliegenId != null){
	 	$query->equalTo("objectId", $mAnliegenId);	
	 }
	 // $query->equalTo("email", "email@me.com");
	 $results = $query->find($mMaster);
	 $anliegen = array();
	 for ($i = 0; $i < count($results); $i++) {
	 	 $anliegen[$i]['id'] = $results[$i]->getObjectId();
	 	 // echo $anliegen[$i]['id'];
		 $anliegen[$i]['titel'] = $results[$i]->get("titel");
		 $anliegen[$i]['beschreibung'] = $results[$i]->get("beschreibung");
		 $anliegen[$i]['updatedAt'] = $results[$i]->getUpdatedAt()->format('c');
		 $anliegen[$i]['createdAt'] = $results[$i]->getCreatedAt()->format('c');
		 $anliegen[$i]['bild'] = $results[$i]->get("bild");
		 $anliegen[$i]['password'] = $results[$i]->get("password");
		 if ($anliegen[$i]['bild'] != null) {
			$anliegen[$i]['bild_url'] = $anliegen[$i]['bild']->getURL();	
		 }
		 $isPublic = isAktionLive($anliegen[$i]['id']);
		 if ($isPublic == true) {
		 	$anliegen[$i]['public'] = true;
		 }else {
		 	$anliegen[$i]['public'] = false;
		 }
		 
	 }
	 return json_encode($anliegen);
	 } catch (Exception $e){
	    return $e->getMessage();
	 }
}

function getDictionary($mSortCol, $mSortDirec, $mPage, $mPageSize, $mMaster){
	try{
	 if ($mMaster == 'true'){
	 	$mMaster = true;
	 }else {
	 	$mMaster = false;
	 }	 
	 
	 $query = new ParseQuery("Dict");
	 
	 $query->limit(1000);
	 if (isset($mSortCol) && $mSortCol != null) {
	 	if ($mSortDirec == "asc"){
	 		$query->ascending($mSortCol);
	 	}else{
	 		$query->descending($mSortCol);
	 	}
	 	$query->skip(($mPage-1) * $mPageSize);
	 	$query->limit($mPageSize);
	 }else {
	 	$query->descending('lastUpdatedAt');
	 }

	 $results = $query->find($mMaster);
	 $gebete = array();
	 
	 for ($i = 0; $i < count($results); $i++) {
	 	 $gebete[$i]['id'] = $results[$i]->getObjectId();
		 $gebete[$i]['english'] = $results[$i]->get("english");
		 $gebete[$i]['german'] = $results[$i]->get("german");
		 $gebete[$i]['french'] = $results[$i]->get("french");
		 $gebete[$i]['coptic'] = $results[$i]->get("coptic");
		 $gebete[$i]['description'] = $results[$i]->get("description");
		 $gebete[$i]['example'] = $results[$i]->get("example");
		 $gebete[$i]['grammar'] = $results[$i]->get("grammar");
		 $gebete[$i]['addedBy'] = $results[$i]->get("addedBy");
		 $gebete[$i]['lastEditBy'] = $results[$i]->get("lastEditBy");
		 $gebete[$i]['updatedAt'] = $results[$i]->getUpdatedAt()->format('c');	
	 }

	 return json_encode($gebete);
	 } catch (Exception $e){
	    return $e->getMessage();
	 }
}

function getAllRes(){
	try{
	 
	 $query = new ParseQuery("Res");
	 $results = $query->find(true);
	 $res = array();
	 
	 for ($i = 0; $i < count($results); $i++) {
	 	 $res[$i]['name'] = $results[$i]->get('name');
		 $res[$i]['text'] = $results[$i]->get('text');
		 $res[$i]['id'] = $results[$i]->getObjectId();
		  
		
	 }

	 return json_encode($res);
	 } catch (Exception $e){
	    return $e->getMessage();
	 }
}

function cmpAnliegenCreatedAt($a, $b){
  return strcmp($b['anliegen'][0]['createdAt'], $a['anliegen'][0]['createdAt']);
}

function getAnliegenCount($mAnliegenId){
	try{
	 
	 $query = new ParseQuery("Gebet");
	 if ($mAnliegenId != null){
	 	$query->equalTo("anliegenId", $mAnliegenId);	
	 }

	 $query->limit(1000);
	 $results = $query->count(true);
	 
	 return $results;
	 } catch (Exception $e){
	    return $e->getMessage();
	 }
}

function getSonntage($mAnliegenId) {
	try{
		$today = new DateTime();
		$fridayCut = date_add($today, date_interval_create_from_date_string('3 Days'));
		$query = new ParseQuery("Gebet");
		$query->equalTo("anliegenId", $mAnliegenId);
		$query->ascending("datum");
		$results = $query->find(true);
		$sonntage = array();
		 
		for ($i = 0; $i < count($results); $i++) {
		 	$datum = $results[$i]->get("datum");
		 	$datumString = strtotime($datum->format('c'));
		 	if((date('l', $datumString) == 'Sunday' && ($fridayCut > $datum))
		 		|| ($i == 0 && date('l', $datumString) != 'Sunday' && ($today > $datum))) {
    			$sonntage[] = $datum;
			}
		}
		return json_encode($sonntage);
	 } catch (Exception $e){
	    return $e->getMessage();
	 }
}

function isAktionLive($mAktionId){
	try{
	 $query = new ParseQuery("Anliegen");
	 
	 if ($mAktionId != null){
	 	$query->equalTo("objectId", $mAktionId);	
	 }
	 $aktion = $query->first(true);
	 return ($aktion->getACL()->getPublicReadAccess());
	 } catch (Exception $e){
	    return $e->getMessage();
	 }
}

function getAnbieterByAktion($mAktionId){
	try{
		$query = new ParseQuery("Anliegen");
	 
	 	$query->equalTo("objectId", $mAktionId);
	 	$query->includeKey("anbieter");	
		 
		$aktion = $query->first(true);
		$anbieter = $aktion->get("anbieter");
		return $anbieter;
	 }catch (Exception $e){
	 	return $e->getMessage();
	 }
}

?>