<?php


// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf.php');
include('getdata.php');

class MYPDF extends TCPDF {
	public $anliegen = null;
    
    public function getAnliegenPdf(){
    	if ($this->anliegen != null) {
    		return $anliegen;
    	}

    	if (isset($_GET['datumStart'])) {
			$datumStart = stripslashes($_GET['datumStart']);
		}		

		if (isset($_GET['anliegenId'])) {
			$anliegenId = stripslashes($_GET['anliegenId']);
		}		

		$datumStart = new DateTime($datumStart);
		$datumEnd = clone $datumStart;

		//if not a Sunday adjust datumEnd
		$datumString = strtotime($datumStart->format('c'));
		if(date('l', $datumString) != 'Sunday') {
			$datumEndString = strtotime('next Sunday', $datumString);
			$datumEnd = new DateTime("@" . $datumEndString);
			// var_dump($datumEnd);
			date_sub($datumEnd, date_interval_create_from_date_string('1 Day'));
			// echo "\n";var_dump($datumEnd);
		}else {
			date_add($datumEnd, date_interval_create_from_date_string('6 Days'));
		}

		$anliegen = json_decode(getAnliegen($anliegenId, $datumStart, $datumEnd, "datum", "asc", 1, 7, 'false'), true); 
		return $anliegen;

    }
    //Page header
    public function Header() {
    	$anliegen = $this->getAnliegenPdf();
    	// $type = pathinfo($anliegen[0]['bild_url'], PATHINFO_EXTENSION);
    	// Logo
        $image_file = $anliegen[0]['bild_url'];//K_PATH_IMAGES."/app_icon.png";
        // echo $type;
        $this->Image($image_file, 15, 10, 15, 15, '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', '', 14);
        // Title
        $this->MultiCell(100, 20, $anliegen[0]['anliegen'][0]['titel'], 0, 'L', 0, false, 35, 10, true, 1, false, true, 55, 'T', true);
        // $this->MultiCell(20, 20, $anliegen[0]['anliegen'][0]['titel'], 0, 0, 'L', 0, '', 0, false, 'M', 'M');
        // $this->SetFont('helvetica', '', 10);
        // $this->Cell(0, 10, "Powered by", 0, 2, 'R', 0, '', 0, false, 'M', 'M');
        // $this->Cell(0, 10, "deutschlandbetet.de", 0, false, 'R', 0, '', 0, false, 'M', 'M');
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        //Deutschland betet logo
        $logo = K_PATH_IMAGES."/app_icon.png";
        $this->Image($logo, 15, $this->GetY() - 15, 15, 15, '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->MultiCell(80, 8, 'Powered by <a href="http://www.deutschlandbetet.de">deutschlandbetet.de</a>', 0, 'L', false, 0, 35, $this->GetY()+12, true, 1, true, true, 8, 'T', true);
        // Page number
        $this->MultiCell(0, 8, 'Seite '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, 'R', false, 0, 0, $this->GetY(), true, 1, false, true, 8, 'T', true);
        // Cell(40, 15, 'Seite '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 1, false, 'R', 0, '', 0, false, 'B', 'B');
    }

}

if (isset($_GET['pdf_titel'])) {
	$pdfTitel = stripslashes($_GET['pdf_titel']);
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$anliegen = $pdf->getAnliegenPdf();
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Deutschland betet');
$pdf->SetTitle('Deutschland betet');
$pdf->SetSubject('Wöchentliche Gebetsanliegen');
$pdf->SetKeywords('Deutschland betet');

// set default header data
// $pdf->SetHeaderData("/app_icon.png", 30, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();

// set cell padding
$pdf->setCellPaddings(1, 1, 1, 1);

// set cell margins
$pdf->setCellMargins(1, 1, 1, 1);

// set color for background
$pdf->SetFillColor(255, 255, 127);

// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)

// $datumStart = new DateTime($datumStart);
// $datumEnd = clone $datumStart;

// //if not a Sunday adjust datumEnd
// $datumString = strtotime($datumStart->format('c'));
// if(date('l', $datumString) != 'Sunday') {
// 	$datumEndString = strtotime('next Sunday', $datumString);
// 	$datumEnd = new DateTime($datumEndString);
// 	date_sub($datumEnd, date_interval_create_from_date_string('2 Days'));
// }else {
// 	date_add($datumEnd, date_interval_create_from_date_string('6 Days'));
// }



//getAnliegen($mAnliegenId, $mDatumStart, $mDatumEnd, $mSortCol, $mSortDirec, $mPage, $mPageSize)

// $anliegen = json_decode(getAnliegen($anliegenId, $datumStart, $datumEnd, "datum", "asc", 1, 7), true); 

// json_decode(exec (' php getdata.php m="anliegen" anliegenId="bNqBk8cvSl"'), true);
// $anliegen = json_decode(exec (' php getparseserver.php'), true);

// TCPDF::MultiCell	(	 	
// 	$w,
//  	$h,
//  	$txt,
//  	$border = 0,
//  	$align = 'J',
//  	$fill = false,
//  	$ln = 1,
//  	$x = '',
//  	$y = '',
//  	$reseth = true,
//  	$stretch = 0,
//  	$ishtml = false,
//  	$autopadding = true,
//  	$maxh = 0,
//  	$valign = 'T',
//  	$fitcell = false 
// )
// writeHTMLCell	(	 	
// 	$w,
//  	$h,
//  	$x,
//  	$y,
//  	$html = '',
//  	$border = 0,
//  	$ln = 0,
//  	$fill = false,
//  	$reseth = true,
//  	$align = '',
//  	$autopadding = true 
// )		

//Beschreibung
//$anliegen[0]['anliegen'][0]['titel']. "\n".
$cellText = "<i>".$anliegen[0]['anliegen'][0]['beschreibung']."</i>";
$pdf->writeHTMLCell(90, 55, '', '', $cellText, 1, 0, false, true, 'L', true);
// $pdf->MultiCell(90, 40, $cellText, 1, 'L', 0, 0, '', '', true, 1, false, true, 55, 'T', true);

 // writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=false, $reseth=true, $align='', $autopadding=true) {
 //     MultiCell($w, $h, $html, $border, $align, $fill, $ln, $x, $y, $reseth, 0, true, $autopadding, 0, 'T', false);
	// }

for ($i = 0; $i < count($anliegen); $i++) {
	if ($i % 2 != 0) {
		//ungerade
		$ln = 0;//1;
	} else {
		//gerade
		$ln = 1;//0;
	}
	$anl = $anliegen[$i];
	// $titel = "<b>".$anl['titel']."</b>";
	$titel = $anl['titel'];
	$datum = new DateTime($anl['datum']);
	$datum = $datum->format('d.m.Y');
	$kurztext = $anl['kurztext'];
	if (strlen($kurztext) < 5) {
		$kurztext = $anl['text'];
	}
	// $cellText = $titel. "<br>".$datum. "<br>".$kurztext;
	$cellText = $titel. "\n".$datum. "\n".$kurztext;
	// $pdf->writeHTMLCell(90, 55, '', '', $cellText, 1, $ln, 0, true, 'L', true);
	$pdf->MultiCell(90, 40, $cellText, 1, 'L', 0, $ln, '', '', true, 1, false, true, 55, 'T', true);
	// $pdf->writeHTMLCell(90, 40, '', '', $cellText, 1, $ln, 0, true, 'L', true);
}
// $pdf->MultiCell(100, 40, $anliegen[0]['titel'], 1, 'L', 1, 0, '', '', true);
// $pdf->MultiCell(55, 5, '[RIGHT] '.$txt, 1, 'R', 0, 1, '', '', true);
// $pdf->MultiCell(55, 5, '[CENTER] '.$txt, 1, 'C', 0, 0, '', '', true);
// $pdf->MultiCell(55, 5, '[JUSTIFY] '.$txt."\n", 1, 'J', 1, 2, '' ,'', true);
// $pdf->MultiCell(55, 5, '[DEFAULT] '.$txt, 1, '', 0, 1, '', '', true);

$pdf->Ln(4);

// set color for background
$pdf->SetFillColor(220, 255, 220);

// Vertical alignment
// $pdf->MultiCell(55, 40, '[VERTICAL ALIGNMENT - TOP] '.$txt, 1, 'J', 1, 0, '', '', true, 0, false, true, 40, 'T');
// $pdf->MultiCell(55, 40, '[VERTICAL ALIGNMENT - MIDDLE] '.$txt, 1, 'J', 1, 0, '', '', true, 0, false, true, 40, 'M');
// $pdf->MultiCell(55, 40, '[VERTICAL ALIGNMENT - BOTTOM] '.$txt, 1, 'J', 1, 1, '', '', true, 0, false, true, 40, 'B');

$pdf->Ln(4);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// set color for background
$pdf->SetFillColor(215, 235, 255);

// set some text for example
$txt = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed imperdiet lectus. Phasellus quis velit velit, non condimentum quam. Sed neque urna, ultrices ac volutpat vel, laoreet vitae augue. Sed vel velit erat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras eget velit nulla, eu sagittis elit. Nunc ac arcu est, in lobortis tellus. Praesent condimentum rhoncus sodales. In hac habitasse platea dictumst. Proin porta eros pharetra enim tincidunt dignissim nec vel dolor. Cras sapien elit, ornare ac dignissim eu, ultricies ac eros. Maecenas augue magna, ultrices a congue in, mollis eu nulla. Nunc venenatis massa at est eleifend faucibus. Vivamus sed risus lectus, nec interdum nunc.

Fusce et felis vitae diam lobortis sollicitudin. Aenean tincidunt accumsan nisi, id vehicula quam laoreet elementum. Phasellus egestas interdum erat, et viverra ipsum ultricies ac. Praesent sagittis augue at augue volutpat eleifend. Cras nec orci neque. Mauris bibendum posuere blandit. Donec feugiat mollis dui sit amet pellentesque. Sed a enim justo. Donec tincidunt, nisl eget elementum aliquam, odio ipsum ultrices quam, eu porttitor ligula urna at lorem. Donec varius, eros et convallis laoreet, ligula tellus consequat felis, ut ornare metus tellus sodales velit. Duis sed diam ante. Ut rutrum malesuada massa, vitae consectetur ipsum rhoncus sed. Suspendisse potenti. Pellentesque a congue massa.';

// print a blox of text using multicell()
// $pdf->MultiCell(80, 5, $txt."\n", 1, 'J', 1, 1, '' ,'', true);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// AUTO-FITTING

// set color for background
$pdf->SetFillColor(255, 235, 235);

// Fit text on cell by reducing font size
// $pdf->MultiCell(55, 60, '[FIT CELL] '.$txt."\n", 1, 'J', 1, 1, 125, 145, true, 0, false, true, 60, 'M', true);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// CUSTOM PADDING

// set color for background
$pdf->SetFillColor(255, 255, 215);

// set font
$pdf->SetFont('helvetica', '', 8);

// set cell padding
$pdf->setCellPaddings(2, 4, 6, 8);

$txt = "CUSTOM PADDING:\nLeft=2, Top=4, Right=6, Bottom=8\nLorem ipsum dolor sit amet, consectetur adipiscing elit. In sed imperdiet lectus. Phasellus quis velit velit, non condimentum quam. Sed neque urna, ultrices ac volutpat vel, laoreet vitae augue.\n";

// $pdf->MultiCell(55, 5, $txt, 1, 'J', 1, 2, 125, 210, true);

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output($pdfTitel.'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+