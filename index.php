<?php $auth_failed = isset($_GET['error']) && $_GET['error'] == "1"? true: false; ?>
<!doctype html>
<head>
  <meta charset="utf-8">

  <title>Coptic Dictionary</title>
    <?php
    session_start();
    if (isset($_SESSION['parseData']['user'])) 
    {
  ?>
  <meta name="description" content="My Parse App">
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
  <script src="polymer/bower_components/webcomponentsjs/webcomponents.js"></script>
  <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
  <script type="text/javascript" src="js/jquery-dateFormat.min.js"></script>
  <link rel="import" href="polymer/bower_components/polymer/polymer.html">
  <link rel="import" href="polymer/bower_components/paper-button/paper-button.html">
  <link href="polymer/bower_components/paper-drawer-panel/paper-drawer-panel.html" rel="import">
  <link href="polymer/bower_components/paper-header-panel/paper-header-panel.html" rel="import">
  <link href="polymer/bower_components/paper-toolbar/paper-toolbar.html" rel="import">
  <link href="polymer/bower_components/paper-icon-button/paper-icon-button.html" rel="import">
  <link href="polymer/bower_components/paper-material/paper-material.html" rel="import">
  <link href="polymer/bower_components/paper-menu/paper-menu.html" rel="import"> 
  <link href="polymer/bower_components/paper-menu/paper-submenu.html" rel="import"> 
  <link href="polymer/bower_components/paper-item/paper-item.html" rel="import">
  <link href="polymer/bower_components/paper-input/paper-input.html" rel="import">
  <link href="polymer/bower_components/iron-icons/iron-icons.html" rel="import">
  <link href="polymer/bower_components/login-polyform/login-polyform.html" rel="import">
  <link href="polymer/bower_components/paper-card/paper-card.html" rel="import">
  <link href="polymer/bower_components/paper-menu-button/paper-menu-button.html" rel="import"> 
  <link rel="import" href="polymer/bower_components/image-mask/image-mask.html">
  <link href="polymer/bower_components/paper-dropdown-menu/paper-dropdown-menu.html" rel="import">
  

 


  <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>-->
  

  
</head>
<dom-module id="todo-app">
  <style>
  .list {
    padding: 0;
/*    border-right: 1px solid #ccc; */
  }
  
  .list paper-item {
    min-height: 80px;
    border-bottom: 1px solid #dedede;
    background-color: #fafafa;
  }
  
  #todoEntry {
    margin: 0;
    width: 100%;
    max-width: none;
    border-bottom: 1px solid #ccc;
  }
  paper-menu {
    cursor: pointer;
  }
  paper-input {
    margin: 0 4vw;
    padding: 0;
  }
  
  paper-material {
    background-color: #fff;
    /*max-width: 640px;*/
    width: 95%;
    margin: 50px auto;
    position: relative;
  }
  
  #todos {
    overflow-y: scroll;
    height: 66vh;
  }
  #todos {
    overflow-y: scroll;
  }
  
  paper-fab {
      position: absolute;
      background: #00BCD4;
      bottom: 2vh;
      right: 2vw;
    }
  .main-panel {
    background-color: #eee;
    height: 100vh;
    overflow-y: hidden;
  }
  
  .task-empty paper-item {
    text-align: center;
    padding: 25px;
    color: #6f6f6f;
  }
  paper-toolbar {
    background:#D32F2F;
  }
  
  #made-with {
    width:100%;
    margin:0;
    position: fixed;
    bottom: 0;
    text-align: center;
  }
  
  #made-with img {
    width: 35%;
    padding: 10px;
  }

  paper-button.fancy:hover {
    background: lime;
  }

  .abmeldenButton {

  }

  .logo {
    margin-right: 5px;
  }

  /*.rightMenu {
    margin-top: 0px;
    max-width: 1000px;
  }*/

  </style>
  
 
<body>
  
  <div id="main">
  <template is="dom-bind" id="app">

    <paper-drawer-panel>
      <paper-header-panel drawer>
        <paper-toolbar></paper-toolbar>
        <div>
          <paper-menu class="mainMenu">
            <paper-button class="startButton" on-click="startClicked">Start</paper-button>
            <paper-button class="DictionaryButton" on-click="dictionaryClicked">Dictionary</paper-button>
          </paper-menu>
        </div>
      </paper-header-panel>
        <paper-header-panel main>
          <paper-toolbar>
            <paper-icon-button icon="menu" paper-drawer-toggle></paper-icon-button>
              <span class="title">
                <img class="logo" src="img/logo.jpg" height="42" width="42">Coptic Dictionary
              </span>

            <paper-menu-button class="rightMenu" horizontal-align="right">
              <paper-icon-button icon="menu" class="dropdown-trigger"></paper-icon-button>
              <paper-menu class="dropdown-content" >
                <template is="dom-repeat" items="{{anbieter}}">
                <paper-button on-tap="meinProfilClicked">
                  <iron-icon src="{{item.bild_url}}"></iron-icon>
                   <?php
                      if ($_SESSION['level'] == "admin") {
                    ?>
                      <span>{{item.name}}</span>
                  <?php
                    }else {?>
                       <span>Profil bearbeiten</span>
                  <?php
                    }
                  ?>
                </paper-button>
              </template>
                <paper-button class="abmeldenButton" on-click="abmeldenClicked">Abmelden</paper-button>
              </paper-menu>
            </paper-menu-button>
            <!-- <paper-icon-button icon="{{app.anbieter.bild_url}}" class="abmeldenButton" on-click="abmeldenClicked">Abmelden</paper-icon-button> -->
          </paper-toolbar>
        <div class="content">
          <template is="dom-repeat" items="{{anbieter}}">
            <paper-card name="statAnbieterCard" heading="Eigene Statistiken">
              <div class="card-content">
                <div class="icon"><img src="{{item.bild_url}}" height="100" width="100" alt="Icon" /></div>
                <!-- <h1>Deutschland Betet Statistiken</h1> -->
                <div class="anbieterresults"></div>
                <div class="anbieterspinner"></div>
              </div>
            </paper-card>
          </template>
        <!-- <p> -->
          <paper-card name="statCard" heading="Coptic Dictionary Statistiken">
            <div class="card-content">
              <div class="icon"><img src="img/logo.jpg" height="100" width="100" alt="Icon" /></div>
              <!-- <h1></h1> -->
              <div class="results"></div>
              <div class="spinner"></div>
            </div>
          </paper-card>
        </div>
        
      </paper-header-panel>
    </paper-drawer-panel>
    <div class="status"></div>
    </template>
</div>
  <script>
  //comment to enable console.log
  // console.log = function() {}

  var app = document.querySelector('#app');
  
  var evt = document.createEvent("Event");
  evt.initEvent("anbieterReady",true,true); 

  document.addEventListener('WebComponentsReady', function() {
    <?php
      if ($_SESSION['level'] == "admin") {?>
        console.log('Admin here!!');
      <?php 
      }
    ?>
    // app.anbieter = 
    // $.post( "getdata.php", {'m': 'anbieter'}, function( result) {
    //   app.anbieter = jQuery.makeArray( result );
    //   document.dispatchEvent(evt);
    // }, "json");  

    $('.startButton').active = true;
    $('.spinner').html('<img src="img/gears.gif" alt="Wait" />');
    $('.anbieterspinner').html('<img src="img/gears.gif" alt="Wait" />');
    $('.results').load('getactivity.php', null, function() {
      $('.spinner').html('');
    });
  });  
  
  // document.addEventListener('anbieterReady', function() {
  //   console.log('anbieterReady');
  //   $.post('getactivity.php', {'anbieter': app.anbieter[0].id}, function(result) {
  //     $('.anbieterresults').html(result);
  //     $('.anbieterspinner').html('');
  //   });
  // });


  app.abmeldenClicked = function(e) {
    console.log("click");
    $.post('logout.php', null, function(success) {
      console.log(success);
      if (success == "true") {
        console.log("abmelden true");
        location.reload();
      }else {
        console.log("abmelden false");
      }
            // $('.login-form').html('Logged In');
          });
  }

  app.meinProfilClicked = function(e) {
     <?php
        if ($_SESSION['level'] == "admin") {?>
          $('.content').load("editProfile.php?id="+e.model.item.id);
          // console.log("editProfile.php?id="+e.model.item.id);
        <?php 
        }else {?>
           $('.content').load("editProfile.php");
        <?php
        }
        ?>
  }

  app.meineAktionenClicked = function(e) {
    $('.content').load("meineAktionen.php");
    app.setButtonsInactive();
    $('.meineAktionenButton').active = true;
  }

  app.dictionaryClicked = function(e) {
    $('.content').load("dictionary.php");
    app.setButtonsInactive();
    $('.dictionaryButton').active = true;
  }
  app.hilfeClicked = function(e) {
    console.log("hilfe clicked");
    // e.preventDefault();
    window.location.href = 'help/Anleitung_Deutschland_betet.pdf';
    app.setButtonsInactive();
    $('.hilfeButton').active = true;
  }

  app.startClicked = function(e) {
    window.location.href = "index.php";
    app.setButtonsInactive();
    $('.startButton').active = true;
  }

  app.setButtonsInactive = function(e) {
   $('.meineAktionenButton').active = false; 
   $('.dictionaryButton').active = false;
   $('.startButton').active = false;
   $('.hilfeButton').active = false;
  }

  app.neuerAnbieterClicked = function(e) {
    $('.content').load("addNew--Anbieter.php");
  }

  app.apptexteClicked = function(e) {
    $('.content').load("apptexte.php");
  }
  
  // app.handleLogin = function(e) {
  //   console.log('login', e.detail);
  //   console.log(this._user);
  // };

    </script>

    <?php
  }else{
    header('Location: login.php');
  }
?>
</body>

</html>
