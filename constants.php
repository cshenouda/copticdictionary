<?php
if (!function_exists('getAnbieterWelcomeText')) {
	function getAnbieterWelcomeText($returnNewAnb){
		return 'Erfolgreich!!<br><br>Sende eine Email an: '.$returnNewAnb['email']
				.'<br>______________________________<br><br>Sehr geehrtes Team '.$returnNewAnb['name']
		        .', <br>Wir freuen uns sehr, dass Sie deutschlandbetet.de als Plattform nutzen möchten.'
		        .'<br>Die Admin-Oberfläche zur Erstellung Ihrer Anliegen finden Sie hier:'
		        .'<br>www.deutschlandbetet.de/admin'
		        .'<br><br>'
		        .'Ihre Zugangsdaten:'
		        .'<br>Benutzername: '.$returnNewAnb['username']
		        .'<br>Passwort: ' .$returnNewAnb['password']
		        .'<br><br>Anbei finden Sie eine Anleitung, wie Anliegen erstellt, editiert, usw. werden. Die gleiche Anleitung finden Sie auch im Adminbereich unter "Hilfe".'
		        .'<br>Wir haben ein Profil und eine Aktion für Sie angelegt. Die weiteren Details dazu (Adresse, Beschreibungen, etc.) können Sie leicht selbst vervollständigen, siehe Anleitung.'
		        .'<br>Ihre Aktion ist momentan nicht öffentlich sichtbar. Sobald Sie live gehen möchten, schreiben Sie uns eine kurze Mail und ich schalte Sie frei.'
		        .'<br><br>Ich bräuchte von Ihnen noch ein Logo für Sie als Anbieter und eins für die aktuell geplante Aktion. Diese können auch identisch sein. '
		        .'<br>Am besten ist es, wenn das Logo einen weißen Hintergrund hat. Es muss außerdem quadratisch sein und mindestens die Größe 128x128 Pixel haben.'
		        .'<br><br>Falls Sie Fragen haben, stehen wir gerne zur Verfügung. '
		        .'<br><br>Freundliche Grüße,'
		        .'<br>- Deutschland betet Team';
	}	
}

if (!function_exists('getNewsletterMailConfirmationText')) {
	function getNewsletterMailConfirmationText($anrede, $confirmationLink){
		return 'Hallo '.$anrede. ', '
			.'<br><br>vielen Dank, dass Sie mitbeten wollen. '
			.'<br>Um die Anmeldung zu bestätigen und um sicher zu gehen, dass niemand anderes in Ihrem Namen Ihre E-Mail-Adresse '
			.'bei uns hinterlassen hat, klicken Sie bitte auf folgenden Link: <a href="' .$confirmationLink. '">' .$confirmationLink. '</a>';
	} 	
}

if (!function_exists('getEditDataMailConfirmationText')) {
	function getEditDataMailConfirmationText($anrede, $confirmationLink){
		return 'Hallo '.$anrede. ', '
			.'<br><br>Ihre Daten bei "Deutschland betet" wurden geändert. '
			.'<br>Sie können Ihre Newsletter-Einstellungen unter folgendem Link einsehen: <br><a href="' .$confirmationLink. '">' .$confirmationLink. '</a>';
	} 	
}

if (!function_exists('getNewsletterMailDaily')) {
	function getNewsletterMailDaily($abos, $unsubscribeLink, $mailLink, $trackingLink){
		$text = '<table style="width: 100%; background-color: #efefef;" cellpadding="10px">'
			.'<tbody>'
			.'<tr>'
			.'<td><div style="text-align: center;">Falls Sie Probleme mit der Darstellung dieser Email haben, <a href="'.$mailLink.'">klicken Sie bitte hier.</a><br /></div>'
			.'<table style="background-color: #ffffff; border-color: #1e73be; width: 600px;" cellpadding="5px">'
			.'<tbody>'
			.'<tr style="background-color: #1e73be;">'
			.'<td>'
			.'<h1><strong><span style="color: #ffffff;"><img src="http://deutschlandbetet.de/wp-content/uploads/2017/02/dbetet_header.png" alt="Deutschland betet Newsletter" width="600px" height="204" /></span></strong></h1>'
			.'</td>'
			.'</tr>'
			.'<tr style="background-color: #ffffff;">'
			.'<td>';
			//Loop Aktionen
			foreach($abos as $abo){
				$text .= '<h1><span style="color: #1e73be;"><img src="'.$abo['aktion']['bild_url'].'" width="42" height="42" />'.$abo['aktion']['titel'].'</span></h1>'
				.'<h3><span style="color: #1e73be;">'.$abo["anliegen"][0]["datum"].' - '.$abo["anliegen"][0]['titel'].'</span></h3>'
				.'<p><span style="color: #000000;">'.nl2br($abo["anliegen"][0]['text']).'<br>'.nl2br($abo["anliegen"][0]['kurztext']).'</span></p>'
				.'<p>&nbsp;</p>'
				.'<hr />';
			}
			$text .= '</td>'
			.'</tr>'
			//Footer
			.'<tr style="background-color: #ccc;">'
			.'<td>'
			.'<table width="564">'
			.'<tbody>'
			.'<tr>'
			.'<td width="564">'
			.'<h4 style="margin:0px;">Tr&auml;gerschaft der GebetsAPP "Deutschland betet" und von www.deutschlandbetet.de</h4>'
			.'<small>Verantwortlich sind der "<strong>Runde Tisch Gebet"</strong>&nbsp;der <strong>Koalition f&uuml;r Evangelisation e.V.</strong> der Lausanner Bewegung in Deutschland in Partnerschaft mit dem Verein "<strong>danken.feiern.beten.e.V."</strong></small>'
			.'</td>'
			.'</tr>'
			.'<tr>'
			.'<td width="564">'
			.'<small>Inhaltlich Verantwortlicher gem&auml;&szlig; &sect; 10 Absatz 3 MDStV</small>'
			.'<ul style="margin:0px;">'
			.'<li><small>f&uuml;r die Inhalte der Gebetsanliegen auf der APP und der Website ist der jeweilige Anbieter der Gebetsanliegen</small></li>'
			.'</ul>'
			.'<ul>'
			.'<li><small>f&uuml;r die allgemeinen Teile der APP und Website ist der Runde Tisch Gebet:</small></li>'
			.'</ul>'
			.'<small><a href="mailto:info@40tagebetenundfasten.de">Bernd Oettinghaus<br /></a>'
			.'Hohemarkstra&szlig;e 8<br />'
			.'60439 Frankfurt am Main</small>'
			.'<h4 style="margin-bottom:0px;">Spenden</h4>'
			.'<small>Wenn Sie die Arbeit an dem Angebot "Deutschland betet" auf der APP und Website mittragen m&ouml;chten, k&ouml;nnen Sie Ihre Spende (steuerlich abzugsf&auml;hig) auf folgendes Konto &uuml;berweisen:<br />'
			.'<a href="http://www.3-oktober.de/"><strong>Danken.Feiern.Beten.e.V.</strong></a></small>'
			.'<ul style="margin:0px;">'
			.'<li style="margin:0px;"><strong><small>IBAN: DE05 3506 0190 1570 6790 11</small></strong></li>'
			.'</ul>'
			.'<small><strong>Verwendungszweck:&nbsp;</strong>'
			.'"Spende GebetsAPP" und Ihre Adresse angeben!</small>'
			.'<p><strong><em>Haftungshinweis</em></strong><em>:&nbsp;Wir &uuml;bernehmen keine Haftung f&uuml;r die Inhalte externer Links. F&uuml;r den Inhalt der verlinkten Seiten sind ausschlie&szlig;lich deren Betreiber verantwortlich.</em></p>'
			.'</td>'
			.'</tr>'
			.'</tbody>'
			.'</table>'
			.'<p><span style="color: #000000;"></span>M&ouml;chten Sie keine Newsletter mehr im Zusammenhang mit dieser Gebetsaktion bekommen, <a href="'.$unsubscribeLink.'" target="_blank">klicken Sie bitte hier</a>.&nbsp;</p>'
			.'</td>'
			.'</tr>'
			.'</tbody>'
			.'</table>'
			.'</td>'
			.'</tr>'
			.'</tbody>'
			.'</table><img src="'.$trackingLink.'"/>';
			return $text;
	}
}
?>