<!doctype html>
<head>
  <meta charset="utf-8">

  <title>Coptic Dictionary</title>
  <?php
    session_start();
    if (!isset($_SESSION['parseData']['user'])) 
    {
  ?>
  <meta name="description" content="My Parse App">
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="css/reset.css">
  <script src="polymer/bower_components/webcomponentsjs/webcomponents-lite.js"></script>
  <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
  
  <link rel="import" href="polymer/bower_components/polymer/polymer.html">
  <link rel="import" href="polymer/bower_components/paper-button/paper-button.html">
  <link href="polymer/bower_components/paper-drawer-panel/paper-drawer-panel.html" rel="import">
  <link href="polymer/bower_components/paper-header-panel/paper-header-panel.html" rel="import">
  <link href="polymer/bower_components/paper-toolbar/paper-toolbar.html" rel="import">
  <link href="polymer/bower_components/paper-icon-button/paper-icon-button.html" rel="import">
  <link href="polymer/bower_components/paper-material/paper-material.html" rel="import">
  <link href="polymer/bower_components/paper-menu/paper-menu.html" rel="import"> 
  <link href="polymer/bower_components/paper-item/paper-item.html" rel="import">
  <link href="polymer/bower_components/paper-input/paper-input.html" rel="import">
  <link href="polymer/bower_components/iron-icons/iron-icons.html" rel="import">
  <link href="polymer/bower_components/login-polyform/login-polyform.html" rel="import">
  <link rel="import" href="polymer/bower_components/iron-a11y-keys/iron-a11y-keys.html">

  
</head>
<style>
  .loginform {
    -webkit-box-align: center;
    -webkit-box-pack: center;
    display: -webkit-box;    
  }

</style>
<body>
  
  <div id="main">
    <template is="dom-bind" id="app">
      <iron-a11y-keys
        id="keys"
        keys="enter"
        target="{{target}}"
        on-keys-pressed="updatePressed">
      </iron-a11y-keys>
      <div class="loginform">
        <login-polyform on-login="handleLogin" credentials="{{_user}}" passwordInputLabel="Passwort" Heading="Anmeldung" submit="Anmelden" usernameInputLabel="Benutzername">
        </login-polyform>
      </div>
    <div class="result">
      
    </div>
    </template>
  </div>
  
  <script>

  var app = document.querySelector('#app');
  
  app.target = this.$.loginform;

  app.updatePressed = function(e) {
    app.handleLogin();
  };
//   window.onkeyup = function(e) {
//    var key = e.keyCode ? e.keyCode : e.which;
//    alert(key + ' pressed. username: ' + $('#username'));
//    if (key == 13) {
//        app.handleLogin();
//    }
//  }
  

  app.handleLogin = function(e) {
  $('.result').html('Prüfen ...');
  $.post( "login_check.php", {'user': this._user.username, 'pass': this._user.password}, function( data ) {
      // data = JSON.parse(data);
      // console.log(data);
      // $('.result').html(data.error);
      if (data.error == null){
        // $('.result').html(data.user);
        console.log(data.user);
        console.log(data.level);
        $('.result').html('Anmelden ...');
        location.reload();
      }else {
        $('.result').html('Fehler! Bitte noch einmal anmelden.');
      }
    }, "json");
    
    // $('.result').load('login_check.php', {'user': this._user.username, 'pass': this._user.password}, function() {
    //         // $('.login-form').html('Logged In');
    //       });
  };
</script>

<?php
  }else{
    header('Location: index.php');
  }
?>
</body>
</html>