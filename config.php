<?php
require 'app/vendor/autoload.php';
include ('constants.php');

use Parse\ParseClient;
use Parse\ParseException;
use Parse\ParseACL;

session_start();

date_default_timezone_set('UTC');

$superAdmins = array ('chrissshen');
$server = "http://caiser-solutions.de:1339/vermittlung"; //json_decode(exec (' php getparseserver.php'), true);

$defaultACL = new ParseACL();
$defaultACL->setPublicReadAccess(true);
$defaultACL->setPublicWriteAccess(false);

// if ($server['appId'] == 'dev'){
// 	$masterKey = 'zvrvZXpV8AOO02jDKj5r85t5H76PY48f2VFBucEK';
// 	$appId = 'BcDbRCvlMoVHdsay7VRWRmMDeRvS9qITsmynasnQ';
// 	$restKey = 'XMfkgMqMl4DDP2jEPa2wCQVAav6fsGdXjPH8hCYS';
// }else {
	$masterKey = '42QPM3FBGTsRptQL07g7F7xLvOL0Dp4MDQKL89tw';
	$appId = 'iTCRrByfZ0MosC8FgxXsrdqmcxkmGTmstG9GHX06';
	$restKey = 'ISQwlTwdlBrae64Tr9IK1KRhgmwnWtf8oOjWsLO7';
// }

ParseClient::initialize($appId, $restKey, $masterKey);

// if ($server['server'] != "parse.com") {
	ParseClient::setServerURL($server);
// }
if (!function_exists('getProperDateFormat')) {
	function getProperDateFormat($value){
		$dateFormatString = 'Y-m-d\TH:i:s.u';
	    $date = date_format($value, $dateFormatString);
	    $date = substr($date, 0, -3) . 'Z';
	    return $date;
	  }
}

?>