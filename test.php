<?php
include 'mcrypt.php';

echo "test";

$responses = getData();
$data = new MCrypt("ch.gemail21@goog", "1dd8cc20402652fe0c4a215897b78d9s");
echo "<style>@font-face { font-family: Coptic; src: url('newath.ttf'); }</style>";
echo "<table><tbody>";
foreach ($responses as $response){
	echo "<tr>";
	if (!empty($response->german)){
		echo "<td>".utf8_decode($data->decrypt($response->german))."</td>";  		
	}else {
		echo "<td> </td>";
	}
	
	if (!empty($response->coptic)){
		echo "<td><font face='Coptic'>".utf8_decode($data->decrypt($response->coptic))."</font></td>";  		
	}else {
		echo "<td> </td>";
	}

	if (!empty($response->arabic)){
		echo "<td>".utf8_decode($data->decrypt($response->arabic))."</td>";  		
	}else {
		echo "<td> </td>";
	}
	echo "</tr>";
}

echo "</tbody></table>";

function sendRequest($url, $data) {
  $options = array(
    'http'=>array(
      'method' => 'POST',
      'content' => http_build_query($data)
    )
  );
  $context = stream_context_create($options);
  $result = file_get_contents($url, false, $context);
  // echo "json";
  // echo $result;
  $obj = json_decode($result);
  if ($obj->status == 'error') {
    // echo "There is an error: $obj->message";
    return $obj;
  }
  
  return $obj;
}

function getData (){

  $url = 'http://onecopticlibrary.com/admin/getdata.php';
  $data = array('query' => 'WHERE b.line_id >= 0', 
	'table' => 'offering_of_incense b join responses r on b.response_id = r.response_id', 
	'columns' => 'DISTINCT r.response_id, r.english, r.coptic, r.arabic, r.german, r.last_updated, r.bibreference', 
	'sort' => 'b.line_id LIMIT 0,500', 
	'email' => 'ch.gemail21@goog', 
	'base_hash' => '1dd8cc20402652fe0c4a215897b78d9s');

  $result = sendRequest($url, $data);
  if ($result->status == "fail") {
    print_r($result);
    // return;
  }


  return $result;
  // return json_encode($result);
}
?>