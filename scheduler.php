<?php

include 'getdata.php';
include 'newsletter_helper.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseException;
use Parse\ParseACL;

$query = new ParseQuery("Newsletter_Job");
$query->includeKey('newsletterUser');
$query->equalTo('done', false);
$query->equalTo('pending', false);
$query->lessThan('schedule', new DateTime());
$query->ascending("createdAt");

$queryScheduleNull = new ParseQuery("Newsletter_Job");
$queryScheduleNull->includeKey('newsletterUser');
$queryScheduleNull->equalTo('done', false);
$queryScheduleNull->equalTo('pending', false);
$queryScheduleNull->equalTo('schedule', null);
$queryScheduleNull->ascending("createdAt");

$mainQuery = ParseQuery::orQueries([$query, $queryScheduleNull]);
$jobs = $mainQuery->find(true);
			
// for ($i = 0; $i < count($results); $i++) {
//  	$jobs[$i]['action'] = $results[$i]->get('action');
//  	$jobs[$i]['newsletterUser'] = $results[$i]->get('newsletterUser');
//  	$jobs[$i]['content'] = $results[$i]->get('content');
//  	$jobs[$i]['title'] = $results[$i]->get('title');
// }

$jobCount = count($jobs);
if ($jobCount == 0) {
	echo "No jobs in queue. Exiting..";
}else {
	for ($i = 0; $i < count($jobs); $i++) {
		$job = $jobs[$i];
		echo "Processing job ".$i."/".$jobCount."...\n";

		//set job pending to true, so that if the scheduler is called again while a job is still running from a previous
		//scheduler call, the job won't be processed again
		$job->set('pending', true);
		$job->save(true);

		$job->get("newsletterUser")->fetch();
		//Send Mail
		if ($job->get('action') == "sendMail"){
			if(!sendMail($job)){
				echo 'Message could not be sent.';
			    echo 'Mailer Error: ' . $mail->ErrorInfo;
			    addToLog($job, false);
			}else {
			    echo 'Message has been sent';
			    clearJob($job, true);
			}
		}
	}
}

function clearJob($job, $success){
	$job->set('done', true);
	$job->save(true);

	addToLog($job, $success);
}

function addToLog($job, $success){
	$defaultACL = new ParseACL();
	$defaultACL->setPublicReadAccess(true);
	$defaultACL->setPublicWriteAccess(false);

	$log = new ParseObject("Newsletter_Log");
	$log->set('newsletterJob', $job);
	$log->set('success', $success);
	$log->set('time', new DateTime());
	$log->setACL($defaultACL);
	$log->save(true);

	$job->set('pending', false);
	$job->save(true);
}


?>