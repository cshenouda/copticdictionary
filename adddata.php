<?php
// include ('config.php');
include ('getdata.php');
include ('newsletter_helper.php');

use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseUser;
use Parse\ParseException;
use Parse\ParseSessionStorage;
use Parse\ParseACL;
use Parse\ParseFile;
use Parse\ParseRole;

$method = stripslashes($_POST['m']);
$user = $_SESSION['parseData']['user'];

// try{
// 	$query = new ParseQuery("Anbieter");
// 	$query->equalTo("user", $user);
// 	$anbieter = $query->first(true);
// } catch (Exception $e){

// }

if ($method == 'addDictEntry') {
	try{
		if (isset($_POST['id'])) {
			$dictId = stripslashes($_POST['id']);
		}
		if (isset($_POST['coptic'])) {
			$coptic = stripslashes($_POST['coptic']);
		}
		if (isset($_POST['english'])) {
			$english = stripslashes($_POST['english']);
		}
		if (isset($_POST['german'])) {
			$german = stripslashes($_POST['german']);
		}
		if (isset($_POST['french'])) {
			$french = stripslashes($_POST['french']);
		}
		if (isset($_POST['grammar'])) {
			$grammar = stripslashes($_POST['grammar']);
		}
		if (isset($_POST['description'])) {
			$description = stripslashes($_POST['description']);
		}
		if (isset($_POST['example'])) {
			$example = stripslashes($_POST['example']);
		}
		

		if (isset($dictId) && $dictId != null){
			$gebet = new ParseObject("Dict", $dictId);
		}else {
			$gebet = new ParseObject("Dict");
			
			$gebetACL = new ParseACL();
			$gebetACL->setUserReadAccess($user, true);
			$gebetACL->setUserWriteAccess($user, true);
			$gebetACL->setPublicWriteAccess(false);
			if ($user->get("username") == "chrissshen"){
				$gebetACL->setPublicReadAccess(false);	
			}else {
				$gebetACL->setPublicReadAccess(true);	
			}
			
			$gebet->setACL($gebetACL);
			$gebet->set("addedBy", $user);
		}
		
		$gebet->set("coptic", $coptic);
		$gebet->set("english", $english);
		$gebet->set("german", $german);
		$gebet->set("french", $french);
		$gebet->set("grammar", $grammar);
		$gebet->set("description", $description);
		$gebet->set("example", $example);
		$gebet->set("lastEditBy", $user);

		$gebet->save(true);

		echo json_encode($gebet);
	} catch (Exception $e){
	   echo $e->getMessage();
	}
}else if ($method == 'addAktion') {
	try{
		if (isset($_POST['anliegenId'])) {
			$anliegenId = stripslashes($_POST['anliegenId']);
		}
		if (isset($_POST['titel'])) {
			$titel = stripslashes($_POST['titel']);
		}
		if (isset($_POST['beschreibung'])) {
			$beschreibung = stripslashes($_POST['beschreibung']);
		}
		if (isset($_POST['password'])) {
			$password = stripslashes($_POST['password']);
		}
		if (isset($_SESSION['parseData']['user'])){
			try{
				$query = new ParseQuery("Anbieter");
				$query->equalTo("user", $user);
				$anbieter = $query->first(true);
			} catch (Exception $e){

			}
		}
		
		if ($anliegenId != null){
			$anliegen = new ParseObject("Anliegen", $anliegenId);
		}else {
			$anliegen = new ParseObject("Anliegen");
		}
		
		if ($anbieter != null) {
			$anliegen->set("anbieter", $anbieter);	
		}
		if ($password != null) {
			$anliegen->set("password", $password);	
		}
		$anliegen->set("titel", $titel);
		$anliegen->set("beschreibung", $beschreibung);

		//bild
		$bild = $_POST['bild'];
		if ($bild != null) {
			$bildFile = ParseFile::createFromFile("user_uploads/".$bild, "bild.jpg");
			$anliegen->set('bild', $bildFile);
		}else {
			
			if ($anliegenId != null){
				// $anliegen->fetch(true);
				// $anliegenBild = $anliegen->get('bild');
				// if ($anliegenBild == null){
				// 	$path = "user_uploads/dummy.jpg";
				// 	$bildFile = ParseFile::createFromFile($path, "dummy-user.jpg");
				// 	$anliegen->set('bild', $bildFile);
				// }
			}else {
				$path = "user_uploads/dummy.jpg";
				$bildFile = ParseFile::createFromFile($path, "dummy-user.jpg");
				$anliegen->set('bild', $bildFile);	
			}
		}
		

		//ACL
		if (isset($_POST['isLive']) && isset($anliegenId)) {
			$live = stripslashes($_POST['isLive']);
			if ($live == 'true'){
				$setLive = true;
			}else {
				$setLive = false;
			}
			$anliegen->save(true);
			setAktionLive($anliegenId, $setLive);
		}else {
			$anliegenACL = new ParseACL();
		
			if ($anliegenId != null) {
				$isLive = isAktionLive($anliegenId);
				if ($isLive) {
					$anliegenACL->setPublicReadAccess(true);
				}else {
					$anliegenACL->setPublicReadAccess(false);
				}
			}else {
				$anliegenACL->setPublicReadAccess(false);
			}

			if ($anbieter != null){
				$anliegenACL->setUserReadAccess($anbieter, true);
				$anliegenACL->setUserWriteAccess($anbieter, true);
			}

			$anliegenACL->setPublicWriteAccess(false);
			
			$anliegen->setACL($anliegenACL);

			$anliegen->save(true);
		}

		echo json_encode($anliegen);
	} catch (Exception $e){
	   echo $e->getMessage();
	}
}else if ($method == 'addProfile') {
	try {
		$newAnbieter = false;
		// if (isset($_POST['anbieter'])) {
		// 	$anbieter = json_decode(stripslashes($_POST['anbieter']), true);
		$anbieter = array();
		if (isset($_POST['id'])){
			$anbieter['id'] = $_POST['id'];
		}else {
			$newAnbieter = true;
		}

		//If new Anbieter create user object with password 
		if ($newAnbieter == true) {
			$newUser = new ParseObject("_User");
			$newUser->set('username', $_POST['userName']);

			$newPassword = generatePassword(8);
			$newUser->set('password', $newPassword);
			$newUser->set('email', $_POST['emailAdresse']);
			$newUser->save(true);

			//set dummy data in new profile	
			$anbieter['name'] = $_POST['name'];
			$anbieter['spende_text'] = 'Spendentext';
			$anbieter['beschreibung'] = 'Beschreibung';
			$anbieter['adresse'] = 'Adresse';
			$anbieter['emailAdresse'] = $_POST['emailAdresse'];
			$anbieter['www'] = 'Web';
			$anbieter['events'] = 'Events';
			$anbieter['tel'] = 'Telefonnummer';
			$anbieter['spende_www'] = 'Spende Web';
			$anbieter['bild'] = $_POST['bild'];
		}else {
			$anbieter['name'] = $_POST['name'];
			$anbieter['spende_text'] = $_POST['spende_text'];
			$anbieter['beschreibung'] = $_POST['beschreibung'];
			$anbieter['adresse'] = $_POST['adresse'];
			$anbieter['emailAdresse'] = $_POST['emailAdresse'];
			$anbieter['www'] = $_POST['www'];
			$anbieter['events'] = $_POST['events'];
			$anbieter['tel'] = $_POST['tel'];
			$anbieter['spende_www'] = $_POST['spende_www'];
			$anbieter['bild'] = $_POST['bild'];
		}

		
		//bild
		if (isset($anbieter['bild'])) {
			$bildFile = ParseFile::createFromFile("user_uploads/".$anbieter['bild'], "bild.jpg");
		// }else {
		// 	$path = "img/dummy-user.jpg";
		// 	// $path = "img/prochrist.jpg";

		// 	$bildFile = ParseFile::createFromFile($path, "dummy-user.jpg");
		}

		if ($newAnbieter == true){
			$anb = new ParseObject("Anbieter");
			$anb->set('user', $newUser);

			$anbieterACL = new ParseACL();
			$anbieterACL->setPublicReadAccess(false);
			$anbieterACL->setUserReadAccess($newUser, true);
			$anbieterACL->setUserWriteAccess($newUser, true);
			$anbieterACL->setPublicWriteAccess(false);

			$anb->setACL($anbieterACL);
		}else {
			$anb = new ParseObject("Anbieter", $anbieter['id']);
		}
		$anb->set('name', $anbieter['name']);
		$anb->set('spende_text', $anbieter['spende_text']);
		// $anb->set('user', $anbieter['user']);
		$anb->set('beschreibung', $anbieter['beschreibung']);
		$anb->set('adresse', $anbieter['adresse']);
		$anb->set('emailAdresse', $anbieter['emailAdresse']);
		$anb->set('www', $anbieter['www']);
		$anb->set('events', $anbieter['events']);
		$anb->set('tel', $anbieter['tel']);
		$anb->set('spende_www', $anbieter['spende_www']);

		if (isset($bildFile) && $bildFile != null){
			$anb->set('bild', $bildFile);	
		}
		
		$anb->save(true);
		if ($newAnbieter == true){
			$returnNewAnb = array();
			$returnNewAnb['name'] = $anb->get('name');
			$returnNewAnb['username'] = $newUser->get('username');
			$returnNewAnb['email'] = $anb->get('emailAdresse');
			$returnNewAnb['password'] = $newPassword;

			$welcomeText = 'Erfolgreich!!<br><br>Sende eine Email an: '.$returnNewAnb['email']
			.'<br>______________________________<br><br>Sehr geehrtes Team '.$returnNewAnb['name']
	        .', <br>Wir freuen uns sehr, dass Sie deutschlandbetet.de als Plattform nutzen möchten.'
	        .'<br>Die Admin-Oberfläche zur Erstellung Ihrer Anliegen finden Sie hier:'
	        .'<br>www.deutschlandbetet.de/admin'
	        .'<br><br>'
	        .'Ihre Zugangsdaten:'
	        .'<br>Benutzername: '.$returnNewAnb['username']
	        .'<br>Passwort: ' .$returnNewAnb['password']
	        .'<br><br>Anbei finden Sie eine Anleitung, wie Anliegen erstellt, editiert, usw. werden. Die gleiche Anleitung finden Sie auch im Adminbereich unter "Hilfe".'
	        .'<br>Wir haben ein Profil und eine Aktion für Sie angelegt. Die weiteren Details dazu (Adresse, Beschreibungen, etc.) können Sie leicht selbst vervollständigen, siehe Anleitung.'
	        .'<br>Ihre Aktion ist momentan nicht öffentlich sichtbar. Sobald Sie live gehen möchten, schreiben Sie uns eine kurze Mail und ich schalte Sie frei.'
	        .'<br><br>Ich bräuchte von Ihnen noch ein Logo für Sie als Anbieter und eins für die aktuell geplante Aktion. Diese können auch identisch sein. '
	        .'<br>Am besten ist es, wenn das Logo einen weißen Hintergrund hat. Es muss außerdem quadratisch sein und mindestens die Größe 128x128 Pixel haben.'
	        .'<br><br>Falls Sie Fragen haben, stehen wir gerne zur Verfügung. '
	        .'<br><br>Freundliche Grüße,'
	        .'<br>- Deutschland betet Team';
	        $returnNewAnb['welcomeText'] = $welcomeText;
			echo json_encode($returnNewAnb);
		}else {
			echo (json_encode($anb));	
		}
	}catch (Exception $e){
		$error['error'] = $e->getMessage();
	 	echo json_encode($error);
	}
}else if ($method == 'addNewsletterUser') {
	try{
		if (isset($_POST['firstName'])) {
			$firstName = stripslashes($_POST['firstName']);
		}
		if (isset($_POST['lastName'])) {
			$lastName = stripslashes($_POST['lastName']);
		}
		if (isset($_POST['email'])) {
			$email = stripslashes($_POST['email']);
		}

		if (!empty($_POST['aktion'])) {
			$aktionen = array();
			foreach($_POST['aktion']['aktion'] as $aktion) {
				$aktionen[] = $aktion;
			}
		}

		$defaultACL = new ParseACL();
		$defaultACL->setPublicReadAccess(true);
		$defaultACL->setPublicWriteAccess(false);
		
		//Create newsletterUser
		$newsletteruser = new ParseObject("Newsletter_User");
		$newsletteruser->set('firstName', $firstName);
		$newsletteruser->set('lastName', $lastName);
		$newsletteruser->set('email', $email);
		$newsletteruser->setACL($defaultACL);
		$newsletteruser->save(true);

		//Create newsletterAbos
		foreach ($aktionen as $aktionId) {
			$aktion = ParseObject::create("Anliegen", $aktionId, false);
			$newsletterAbo = new ParseObject("Newsletter_Abo");
			$newsletterAbo->set("newsletterUser", $newsletteruser);
			$newsletterAbo->set("anliegen", $aktion);
			$newsletterAbo->set("intervall", 1);
			$newsletterAbo->set("confirmed", false);
			$newsletterAbo->set("unsubscribeRequested", false);
			$newsletterAbo->set("subscribeRequested", true);
			$newsletterAbo->setACL($defaultACL);
			$newsletterAbo->save(true);
		}

		//Add job for sending confirmation mail
		$newsletterJob = new ParseObject("Newsletter_Job");
		$newsletterJob->set('action', "sendMail");
		$newsletterJob->set('newsletterUser', $newsletteruser);

		$newsletteruser->fetch(true);
		$newsletterJob->set('content', getNewsletterMailConfirmationText(getAnrede($firstName, $lastName), getConfirmationLink($newsletteruser->getObjectId(), true)));
		$newsletterJob->set('title', "Anmeldung zum Deutschland-betet-Newsletter");
		$newsletterJob->set('done', false);
		$newsletterJob->set('pending', false);
		//we're not setting schedule date, because we want the email to be sent immediately
		$newsletterJob->setACL($defaultACL);
		$newsletterJob->save(true);


		echo json_encode($newsletteruser);
	}catch (Exception $e){
		$error['error'] = $e->getMessage();
	 	echo json_encode($error);
	}
}else if ($method == 'addRes') {
	try{
		if (isset($_POST['res'])) {
			$res = $_POST['res'];

			foreach($res as $txt){
				$parseRes = new ParseObject("Res", $txt['id']);
				$parseRes->set("text", $txt['text']);
				$parseRes->save(true);
			}
		}


	}catch (Exception $e){
		$error['error'] = $e->getMessage();
	 	echo json_encode($error);
	}
}else if ($method == 'addToSecureAktion'){
	if (isset($_POST['userId'])) {
		$userId = stripslashes($_POST['userId']);
	}

	if (isset($_POST['pass'])) {
		$pass = stripslashes($_POST['pass']);
	}

	echo addToSecureAktion($userId, $pass);
}else if ($method == 'setAktionSecured'){
	if (isset($_POST['anliegenId'])) {
		$anliegenId = stripslashes($_POST['anliegenId']);
	}

	if (isset($_POST['setSecured'])) {
		$setSecured = stripslashes($_POST['setSecured']);
		if ($setSecured == 'true'){
		 	$setSecured = true;
		 }else {
		 	$setSecured = false;
		 }
	}
	echo setAktionSecured($anliegenId, $setSecured);
}


function addToSecureAktion($userId, $pass){
	$query = new ParseQuery("Anliegen");
	$query->notEqualTo("password", "");
	$query->notEqualTo("password", " ");
	$query->notEqualTo("password", null);
	$results = $query->find(true);
	for ($i = 0; $i < count($results); $i++) {
		if ($pass == md5($results[$i]->get("password"))){
			$secureRole = getSecureRole($results[$i]->getObjectId());
			if ($secureRole == null){
				return "failed";
			}
			
			$user = new ParseObject('_User', $userId);
			$secureRole->getUsers()->add($user);
			$secureRole->save(true);
			return "success";		
		}
	}		

	return "failed";
}

function addSecureRole($anliegenId){
	$roleACL = new ParseACL();
	$roleACL->setPublicReadAccess(true);
	$role = ParseRole::createRole(getSecureRoleName($anliegenId), $roleACL);
	$role->save(true);	
	return $role;
}

function removeSecureRole($anliegenId){
	$secureRole = getSecureRole($anliegenId);
	$secureRole->destroy(true);
	$secureRole->save(true);
}

function getSecureRole($anliegenId){
	$query = new ParseQuery("_Role");
	$query->equalTo("name", getSecureRoleName($anliegenId));
	$foundRole = $query->first(true);
	if ($foundRole != null){
		//Role exists
		return $foundRole;
	}else {
		return null;
	}
}

function getSecureRoleName($anliegenId){
	return $anliegenId."Users";
}

function setAktionSecured($anliegenId, $setSecured){
	try{
		//Create new role that will contain the users that are allowed to view the secured anliegen
		$secureRole = getSecureRole($anliegenId);
		if ($secureRole == null){
			$secureRole = addSecureRole($anliegenId);
		}
		//get Aktion and its ACL
		$aktion = new ParseObject("Anliegen", $anliegenId);
		$aktionACL = new ParseACL();

		$anbieter = getAnbieterByAktion($anliegenId);
		$aktionACL->setUserReadAccess($anbieter->get("user"), true);
		$aktionACL->setUserWriteAccess($anbieter->get("user"), true);
		$aktionACL->setPublicWriteAccess(false);
			
		if ($setSecured == true){
			//add to Aktion's ACL the SecureRole with read permission
			$aktionACL->setPublicReadAccess(false);
			$aktionACL->setRoleReadAccessWithName(getSecureRoleName($anliegenId), true);
			$aktion->setACL($aktionACL);
			$aktion->set("password", generatePassword(4, false));
			$aktion->save(true);	
		}else {
			//add to Aktion's ACL the SecureRole
			$aktionACL->setPublicReadAccess(false);	
			$aktionACL->setRoleReadAccessWithName(getSecureRoleName($anliegenId), false);
			$aktion->setACL($aktionACL);
			$aktion->set("password", "");
			$aktion->save(true);
			//remove secureRole
			removeSecureRole($anliegenId);
			$query = new ParseQuery("Abo");
			$query->equalTo("anliegenId", $anliegenId);
			$results = $query->find(true);
			for ($i = 0; $i < count($results); $i++) {
				$results[$i]->destroy(true);
				$results[$i]->save(true);
			}
		}
		
		//add SecureRole with read perm to all Gebetsanliegen of this Aktion
		$query = new ParseQuery("Gebet");
		if (isset($anliegenId) && $anliegenId != null){
			$query->equalTo("anliegenId", $anliegenId);	
			$results = $query->find(true);

			for ($i = 0; $i < count($results); $i++) {
				$gebetACL = new ParseACL();
				$gebetACL->setUserReadAccess($anbieter->get("user"), true);
				$gebetACL->setUserWriteAccess($anbieter->get("user"), true);
				$gebetACL->setPublicWriteAccess(false);
				if ($setSecured == true){
					$gebetACL->setRoleReadAccessWithName(getSecureRoleName($anliegenId), true);
				}else {
					$gebetACL->setRoleReadAccessWithName(getSecureRoleName($anliegenId), false);
				}
				$gebetACL->setPublicReadAccess(false);	
				$gebetACL->setPublicWriteAccess(false);
				$results[$i]->setACL($gebetACL);
				$results[$i]->save(true);
			}
		}
		
		$pass = $aktion->get("password");
		if ($pass != null){
			$aktion->set("password", md5($pass));
		}
		echo json_encode($aktion);
	} catch (Exception $e){
	   echo $e->getMessage();
	}
}


function isAktionSecured($anliegenId){
	$query = new ParseQuery("Anliegen");
	$query->equalTo("objectId", $anliegenId);
	$aktion = $query->first(true);

	if($aktion != null){
		$pass = $aktion->get("password");
		if ($pass != null && count($pass) > 3){
			return true;
		}else {
			return false;
		}
	}
}

function setAnbieterLive($anliegenId, $setLive){
	//Set Anbieter live status
	$anbieter = getAnbieterByAktion($anliegenId);
	// print_r($anbieter);
	$anbieterACL = new ParseACL();
	if ($setLive == true){
		$anbieterACL->setPublicReadAccess(true);
	}else {
		$anbieterACL->setPublicReadAccess(false);
	}
	$anbieterACL->setUserReadAccess($anbieter->get("user"), true);
	$anbieterACL->setUserWriteAccess($anbieter->get("user"), true);
	$anbieterACL->setPublicWriteAccess(false);
	
	$anbieter->setACL($anbieterACL);
	$anbieter->save(true);
}

function setAktionLive($anliegenId, $setLive){
	try{
		//Set Anbieter live status
		// if ($setLive == true && !isAktionSecured($anliegenId)){
		// 	setAnbieterLive($anliegenId, true);
		// }else {
		// 	setAnbieterLive($anliegenId, false);
		// }
		
		if ($setLive == true && isAktionSecured($anliegenId) == true){
			return;
		}
		//Set Aktion live status
		$anliegen = new ParseObject("Anliegen", $anliegenId);
		
		//ACL
		$anliegenACL = new ParseACL();
		
		if ($setLive == true){
			$anliegenACL->setPublicReadAccess(true);
		}else {
			$anliegenACL->setPublicReadAccess(false);
		}

		$anbieter = getAnbieterByAktion($anliegenId);
		$anliegenACL->setUserReadAccess($anbieter->get("user"), true);
		$anliegenACL->setUserWriteAccess($anbieter->get("user"), true);
		$anliegenACL->setPublicWriteAccess(false);		
		$anliegen->setACL($anliegenACL);
		$anliegen->save(true);

		//Get Gebete of this Aktion and set Live status accordingly
		$query = new ParseQuery("Gebet");
		if (isset($anliegenId) && $anliegenId != null){
			$query->equalTo("anliegenId", $anliegenId);	
			$results = $query->find(true);

			for ($i = 0; $i < count($results); $i++) {
				$gebetACL = new ParseACL();
				if ($setLive == true){
					$gebetACL->setPublicReadAccess(true);
				}else {
					$gebetACL->setPublicReadAccess(false);
				}
				$gebetACL->setUserReadAccess($anbieter->get("user"), true);
				$gebetACL->setUserWriteAccess($anbieter->get("user"), true);
				$gebetACL->setPublicWriteAccess(false);
				$results[$i]->setACL($gebetACL);
				$results[$i]->save(true);
			}
		}

		echo json_encode($anliegen);
	} catch (Exception $e){
	   echo $e->getMessage();
	}
}

function generatePassword($length = 8, $withLowerCase = true) {
	if ($withLowerCase == true){
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';	
	}else {
		$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	}
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}
?>