<?php
include 'constants.php';
include 'getdata.php';
include ('newsletter_helper.php');

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseException;
use Parse\ParseACL;

/*
This script needs to run daily, BEFORE the scheduled time of mail delivery, but on the same calendar day (e.g. if sendmails.php runs at 7am, let this script
run at 5am the same day)
Set a scheduled time of delivery -> $scheduledTimeJobs
Loop through each Newsletter_User's Newsletter_Abos 
 -> we want to combine all Abos that qualify, so user gets only one email
 -> check if lastSent is at least <intervall> days away from <lastSent> (make sure this script runs before scheduled time of mail delivery)
  -> True: Add to array of abos to be sent
 
 -> Combine all Abos: Text using template in constants.php
 -> Create job
*/

$scheduledTimeJobs = new DateTime();
/* 
Set time of scheduler cronjob here!
*/
$scheduledTimeJobs->setTime(7,0);

$defaultACL = new ParseACL();
$defaultACL->setPublicReadAccess(true);
$defaultACL->setPublicWriteAccess(false);

$query = new ParseQuery("Newsletter_User");
$newsletterUsers = $query->find(true);

foreach ($newsletterUsers as $newsletterUser){
	$abosToBeSent = array();

	$query = new ParseQuery("Newsletter_Abo");
	$query->includeKey("anliegen");
	$query->includeKey("newsletterUser");
	$query->equalTo("newsletterUser", $newsletterUser);
	$query->equalTo("confirmed", true);
	$abos = $query->find(true);

	foreach($abos as $abo){
		if ($abo->get("lastSent")!= null && isAboQualified($abo->get("lastSent"), $abo->get("intervall"))){
			//Abo is qualified, add to array
			$abosToBeSent[] = $abo;
		}else if ($abo->get("lastSent") == null){
			//Abo was never sent, so it qualifies, add to array
			$abosToBeSent[] = $abo;
		}
	}

	//test
	// $unsubscribeLink = getUnsubscribeLink($newsletterUser->getObjectId(), false);
	// $mailJobContent = getNewsletterMailDaily(array(), $unsubscribeLink);

	//Create jobs for each qualified abo
	$mail = array();
	if (count($abosToBeSent) == 0){
		echo "No jobs to be created for User ".$newsletterUser->getObjectId()."\n";
	}
	for($i = 0; $i < count($abosToBeSent); $i++){
		$aboToBeSent = $abosToBeSent[$i];
		
		//get today's Anliegen
		$today = new DateTime();
		$anliegen = getAnliegen($aboToBeSent->get('anliegen')->getObjectId(), $today, $today, "datum", "asc", 1, 20, true);
		$anliegen = json_decode($anliegen, true);

		if ($anliegen != null && !empty($anliegen)){
			echo "Found qualified Abo for User ".$newsletterUser->getObjectId().": "
				.$aboToBeSent->getObjectId()." (".$aboToBeSent->get("anliegen")->get("titel").")\n";

			//Set lastSent = scheduledTimeJobs time in Abo
			$today = new DateTime();
			if ($today > $scheduledTimeJobs){
				$lastSent = $today;
			}else {
				$lastSent = $scheduledTimeJobs;
			}
			$aboToBeSent->set("lastSent", $lastSent);
			$aboToBeSent->save(true);
			
			$mail[$i]['anrede'] = getAnrede($newsletterUser->get('firstName'), $newsletterUser->get('lastName'));
			$mail[$i]['aktion']['titel'] = $aboToBeSent->get('anliegen')->get('titel');
			$bild = $aboToBeSent->get('anliegen')->get("bild");
			if ($bild != null){
				$mail[$i]['aktion']['bild_url'] = $bild->getURL();	
			}
			
			for($ii = 0; $ii < count($anliegen); $ii++){
				$mail[$i]['anliegen'][$ii]['titel'] = $anliegen[$ii]['titel'];
				$mail[$i]['anliegen'][$ii]['text'] = $anliegen[$ii]['text'];
				$mail[$i]['anliegen'][$ii]['kurztext'] = $anliegen[$ii]['kurztext'];
				$datum = new DateTime($anliegen[$ii]['datum']);
				$mail[$i]['anliegen'][$ii]['datum'] = $datum->format('d.m.Y');
			}	
		}else {
			//remove this abo from array as it has no Anliegen for today
			echo $aboToBeSent->get("anliegen")->get("titel"). " has no Anliegen today (User: ".$newsletterUser->getObjectId().")\n";
		}		
	}

	if ($mail != null && count($mail) >= 1){
		$unsubscribeLink = getUnsubscribeLink($newsletterUser->getObjectId(), false);
		// echo $mailJobContent;	

		//Create the job
		$newsletterJob = new ParseObject("Newsletter_Job");
		$newsletterJob->set('action', "sendMail");
		$newsletterJob->set('newsletterUser', $newsletterUser);

		$newsletterUser->fetch(true);
		// $newsletterJob->set('content', $mailJobContent);
		$subject = "Deutschland betet am ".$today->format('d.m.Y');
		$newsletterJob->set('title', $subject);
		$newsletterJob->set('done', false);
		$newsletterJob->set('pending', false);
		//we're setting schedule date, because we want the email NOT to be sent immediately
		$newsletterJob->set('schedule', $scheduledTimeJobs);
		$newsletterJob->setACL($defaultACL);
		$newsletterJob->save(true);
		$newsletterJob->fetch(true);

		$mailLink = getMailLink($newsletterUser->getObjectId(), $newsletterJob->getObjectId());
		$trackingLink = getTrackingLink($newsletterJob->getObjectId());
		//Here we get the html mail content
		$mailJobContent = getNewsletterMailDaily($mail, $unsubscribeLink, $mailLink, $trackingLink);
		$newsletterJob->set('content', $mailJobContent);
		$newsletterJob->save(true);
		
		echo "Created job for User ".$newsletterUser->getObjectId().": ".$subject."\n";
	}
	
}

function isAboQualified($lastSent, $interval){
	$lastSent->setTime(0,0,0);
	$today = new DateTime();
	$today->setTime(0,0,0);
	// echo "lastSent: ".$lastSent->format('Y-m-d H:i:s');
	// echo "\ntoday: ".$today->format('Y-m-d H:i:s');

	$cutOff = date_sub($today, dateInterval($interval));
	$cutOff = date_add($cutOff, new DateInterval("PT1S"));
	// echo "\ncutoff: ".$cutOff->format('Y-m-d H:i:s')."\n";
	
	if ($lastSent <= $cutOff){
		return true;
	}else {
		return false;	
	}
}

function dateInterval($days){
	return new DateInterval("P".$days."D");
}
?>